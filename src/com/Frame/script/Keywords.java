package com.Frame.script;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Calendar;









import static com.Frame.script.DriverScript.APP_LOGS;
import static com.Frame.script.DriverScript.CONFIG;
import static com.Frame.script.DriverScript.OR;
import static org.junit.Assert.*;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.Array;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;

import org.apache.commons.io.FileUtils;
/*import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;*/
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.basics.Debug;
import org.sikuli.script.Screen;

import com.Frame.XL.XlsWriter;
import com.sun.corba.se.spi.orbutil.fsm.Action;

public class Keywords {
	String starttime1; 
	String starttime2;
	String window1;
	String window2;
	String expirydate;
	String finalTime;
	String browser;
	public WebDriver driver;
	String spapptimearrpart1;
	String spapptimearrpart3;
	String Gmailapptimearrpart1;
	String Gmailapptimearrpart2;
	String actDueDate;
	String actduemon;
	String SPapptime;
	String time;
	String GmailAppTme;
	String SPAddress;
	String OpenActText;
	String CloseActText;
	String AppTme;
	String date1;
	String actNum;
	String date2;
	String date3;
	boolean fname;
	String actual;
	String expected;
	String APIURL;
	String data1 = "Ashby7070 &";
	String ExYear;
	String ExMonth;
	String ExDate;
	String savedexyear;
	String savedexMonth;
	String savedeexDate;
	//===============================================
	public static String USERNAME = "deepakjaiswal1";
	public static String ACCESS_KEY = "5pGgJqXsJ9yF9dJmT2pN"; 
	public static String BROWSERSTACKURL = "https://"+USERNAME+":"+ACCESS_KEY+"@hub.browserstack.com/wd/hub";
	//===============================================
	//String upload;
	//String randomemail=	"qmerit10+consumer511"+"563"+new Random().nextInt(50)+new Random().nextInt(30)+"tzglbui0@mailosaur.io";
	//String randomemail1="qmerit10+consumer511"+"563"+new Random().nextInt(51)+new Random().nextInt(21)+"@gmail.com";
		
	/**
	 * Generate an unique email account, using prefix, date+time, and suffix
	 * Date: 4/13/2018
	 */
	public String genAcct(String object, String data) {
		try {
			Date d = new Date();
			SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
					"yyyyMMdd_HH_mm_SS");
			String date = DATE_FORMAT.format(d);
			String randomemail = CONFIG.getProperty("CMR_acctPrefix")
					+ CONFIG.getProperty("CMR_acctmid") + "+"
					+ new Random().nextInt(200) + new Random().nextInt(90)
					+ CONFIG.getProperty("CMR_acctSuffix");
			CONFIG.setProperty("CMR_acct", randomemail);
			System.out.println("CMR_acct: " + randomemail);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to generate email account" + e.getMessage();
		}
	}

	public String genProgName(String fileName, String sheetName) {
		try {

			String RandomProgName = CONFIG.getProperty("ProgName")
					+ new Random().nextInt(1000);
			new XlsWriter(System.getProperty("user.dir")
					+ CONFIG.getProperty("PROG_FILE_NAME") + fileName,sheetName).writeExcel(RandomProgName);
			CONFIG.setProperty("Program Name", RandomProgName);
			System.out.println("Program Name1: " + RandomProgName);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to generate Program Name" + e.getMessage();
		}
	}
	
	/*public String GetActivitynumber(String object, String data) {
		APP_LOGS.debug("n");
		String activityNumber=null;
		try {
			 activityNumber = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			CONFIG.setProperty("Activity Num", activityNumber);
			System.out.println("Activity Number is ->"  +activityNumber);
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + "- Not able get actvity number";
		}
		return Constants.KEYWORD_PASS +"    "+" Activity Number is -> null"+activityNumber  ;

	}*/
	
	/**
	 *Purpose : This method is used for Write Activity number to the Excel script file
	 * Usage:
	 * Date: 4/26/18
	 */
	public String WriteActivityNumberinExcel(String object, String data) {
		try {

			if (data != null && data.startsWith(Constants.DIRECT_FROM_SHEET)) {
				String[] arr = data
						.split(Constants.DIRECT_FROM_SHEET_FILE_SHEET_SPLITER);
				if (arr != null && arr.length > 1) {// remove prefix _SH_
					new XlsWriter(System.getProperty("user.dir")
							+ CONFIG.getProperty("PROG_FILE_NAME")
							+ arr[0].substring(4), arr[1])
							.writeExcel(CONFIG.getProperty("Activity Num"));
					return Constants.KEYWORD_PASS;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Constants.KEYWORD_FAIL
					+ " -- unable to write Activity number" + e.getMessage();
		}
		return Constants.KEYWORD_FAIL = " file or sheet not found!!";
	}

	public String enterProgName(String object, String data) {
		APP_LOGS.debug("in ren:");
		if (data != null && data.startsWith(Constants.DIRECT_FROM_SHEET)) {
			String[] arr = data
					.split(Constants.DIRECT_FROM_SHEET_FILE_SHEET_SPLITER);
			if (arr != null && arr.length > 1)
				genProgName(arr[0].substring(4), arr[1]); // remove prefix _SH_
		}
		try {
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(
					CONFIG.getProperty("Program Name"));
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to enter the Requirement" + e.getMessage();
		}
	}

	public String genReqName(String fileName, String sheetName) {
		try {

			String RandomReqName = CONFIG.getProperty("ReqName")
					+ new Random().nextInt(1000);
			new XlsWriter(System.getProperty("user.dir")
					+ CONFIG.getProperty("PROG_FILE_NAME") + fileName,
					sheetName).writeExcel(RandomReqName);
			CONFIG.setProperty("Req Name", RandomReqName);
			System.out.println("Req Name1: " + RandomReqName);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to generate Program Name" + e.getMessage();
		}
	}
	/**
	 *Purpose: This method is used for click on any webelemnt using Action class
	 */
	public String clickusingActionClass(String object, String data) {
		APP_LOGS.debug("clickUsingActionClass ");
		try {
			WebElement btn = driver.findElement(By.xpath(OR.getProperty(object)));
			Actions ac = new Actions(driver);
			ac.click(btn).build().perform();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to click on link"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
	
	
	/**
	 *Purpose: This is a Test case specific method
	 *Generate random requirement name using genReqName() method, and
	 * further Enter it to Requirement Name Field Usage : This routine is
	 * specific to Manage Requirement Test case 10542.
	 */
	public String genReqNameforTC10542(String fileName, String sheetName) {
		try {

			String RandomReqName = CONFIG.getProperty("ReqName")
					+ new Random().nextInt(1000);
			new XlsWriter(System.getProperty("user.dir")
					+ CONFIG.getProperty("PROG_FILE_NAME") + fileName,
					sheetName).writeExcel1(RandomReqName);
			CONFIG.setProperty("Req Name", RandomReqName);
			System.out.println("Req Name1: " + RandomReqName);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to generate Program Name" + e.getMessage();
		}
	}

	/**
	*Purpose: Generate random requirement name using genReqName() method, and
	 * further Enter it to Requirement Name Field Usage : This routine is
	 * specific to Manage Requirement.
	 */
	public String enterReqName(String object, String data) {
		APP_LOGS.debug("in ren:");
		if (data != null && data.startsWith(Constants.DIRECT_FROM_SHEET)) {
			String[] arr = data
					.split(Constants.DIRECT_FROM_SHEET_FILE_SHEET_SPLITER);
			if (arr != null && arr.length > 1)
				genReqName(arr[0].substring(4), arr[1]); // remove prefix _SH_
		}
		try {
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(
					CONFIG.getProperty("Req Name"));
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to enter the Requirement" + e.getMessage();
		}
	}
	
	
	/**
	*Purpose: This is a test case specific method
	*Generate random requirement name using genReqName() method, and
	 * further Enter it to Requirement Name Field Usage : This routine is
	 * specific to Manage Requirement Test case 10542.
	 */
	public String enterReqNameforTC10542(String object, String data) {
		APP_LOGS.debug("in ren:");
		if (data != null && data.startsWith(Constants.DIRECT_FROM_SHEET)) {
			String[] arr = data
					.split(Constants.DIRECT_FROM_SHEET_FILE_SHEET_SPLITER);
			if (arr != null && arr.length > 1)
				genReqNameforTC10542(arr[0].substring(4), arr[1]); // remove prefix _SH_
		}
		try {
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(
					CONFIG.getProperty("Req Name"));
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to enter the Requirement" + e.getMessage();
		}
	}
	
	/**
     *Purpose: hover mouse over object and compare the pop-up tooltip with
	 * expected tooltip. searchTooltip is defined in or.properties file. TODO:
	 * look for a better way to pass in 2nd object instead of hardcoded the
	 * property searchtooltip Usage : This routine is specific to simple search.
	 * Date: 4/26/18
	 */
	public String mousehoverwhatToSearch(String object, String data) {
		try {
			// WebElement scrollElement =
			// driver.findElement(By.xpath(OR.getProperty(object)));
			WebElement element = driver.findElement(By.xpath(OR
					.getProperty(object)));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).build().perform();

			WebElement toolTipElement = driver.findElement(By.cssSelector(OR
					.getProperty("searchtooltip")));
			Thread.sleep(2000);
			// To get the tool tip text and compares
			String toolTipText = toolTipElement.getText();
			System.out.println("Tooltip Text is -> " + toolTipText);
			if (toolTipText.contains(data)) {
				System.out.println("verified : " + "Expected -> " + data
						+ "    " + "Actual -> " + toolTipText);
			} else {
				System.out.println("verified : " + "Expected -> " + data
						+ "    " + "Actual -> " + toolTipText);
				return Constants.KEYWORD_FAIL
						+ " Tooltip text not found or text not match ";
			}
			// builder.click(element).build().perform();
			// mouseOvertext.perform();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to scroll down to the element"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}	
	
	/**
 	 *Purpose: wait for email with the subject (in the data field) to arrive in the inbox
 	 *Date: 4/26/18
 	 */
	public String wait4emailarrival(String object, String data) {
		try {
			//wait for a total of 2 minutes, with polling interval 
			//of 3 seconds, for email to arrive.  Normally it should not
			//take long for email to arrive, but PRD2 is slow
			//and might take a while.
			APP_LOGS.debug("wait4emailarrival: " + data);
			for (int loop = 0; loop < 40; loop++) {
				List<WebElement> a = driver.findElements(By
						.xpath("//*[@class='y6']/span"));
				System.out.println(a.size());
				System.out.println("expected email:" + data);
			    for (int i = 0; i < a.size(); i++) {
				    System.out.println(a.get(i).getText());
				    if (a.get(i).getText().contains(data)) {
					    return Constants.KEYWORD_PASS;
				    }
			    }
			    Thread.sleep(3000);
			}
			return Constants.KEYWORD_FAIL + "cannot find email: " + data + " after 2 minutes";
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ "Wait4emailarrival exception caught: " + data + " after 2 minutes" + e.getMessage();
		}
	}

	/**
 	 *Purpose: This method is used for click on Mail having specific subject
 	 * in the data field
 	 * this method is framework specific
 	 * date: 4/26/18
 	 */
	public String clkgmail(String object, String data) {
		try {
			List<WebElement> a = driver.findElements(By
					.xpath("//*[@class='y6']/span"));
			System.out.println(a.size());
			for (int i = 0; i < a.size(); i++) {
				System.out.println(a.get(i).getText());
				if (a.get(i).getText().contains(data)) {
					a.get(i).click();
					break;
				}
			}
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " Exception-- unable to click on email:" + data
					+ " Exception: " + e.getMessage();
		}
	}
	
	/*
	 * Purpose: Generate random email and enter it in the object
	 * Date: 4/13/2018
	 */
	public String ren(String object, String data) {
		APP_LOGS.debug("in ren:");
		try {
			String result=genAcct(null, null);
			if(!result.equals(Constants.KEYWORD_PASS))
				return Constants.KEYWORD_FAIL + " -- random email id set failed!";
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(CONFIG.getProperty("CMR_acct"));
			//driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(randomemail);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to enter the email id"
					+ e.getMessage();
		}
	}

	public String reqname(String object, String data) {
		APP_LOGS.debug("Generate Random Requirement Name");
		try {
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(CONFIG.getProperty("ranreqname"));
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to close the browser"
					+ e.getMessage();
		}
	}

	public String ren1(String object, String data) {
		APP_LOGS.debug("Closing Browser");
		try {
			//driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(randomemail);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to close the browser"
					+ e.getMessage();
		}
	}

	public String closeBrowser(String object, String data) {
		APP_LOGS.debug("Closing Browser");
		try {

			if (data.equals("Chrome")) {
				driver.close();
			}
			long implicitWaitTime = Long.parseLong(CONFIG
					.getProperty("implicitwait"));
			//driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to close the browser"
					+ e.getMessage();
		}
	}

    /**
	 * Purpose: To close all opened web browser Instance. Using quit() method.
	 */

	public String closemozBrowser(String object, String data) {
		APP_LOGS.debug("Closing Browser");
		try {

			driver.quit();
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to close the browser"
					+ e.getMessage();
		}
	}

    /**
	 * Open A New Browser Instance of browser. We can set in the "Config file"
	 * on Which browser we want to run the script.
	 */

	public String openBrowser(String object, String data) {
		APP_LOGS.debug("Opening browser");
		if (data.equals("Mozilla")) {
			// System.setProperty("webdriver.gecko.driver",
			// "C:\\Users\\rishi.kataria\\Downloads\\geckodriver-v0.8.0-win32\\geckodriver.exe");

			// driver = new MarionetteDriver();
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir")
					+ "\\drivers\\geckodriver.exe");
			driver = new FirefoxDriver();

			driver.manage().window().maximize();
			Set<Cookie> allCookies = driver.manage().getCookies();
			for (Cookie cookie : allCookies) {
				driver.manage().addCookie(cookie);
			}
		} else if (data.equals("IE")) {
			System.setProperty("webdriver.ie.driver",
					System.getProperty("user.dir")
					+ "\\drivers\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
		} else if (data.equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir")
					+ "\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			Set<Cookie> allCookies = driver.manage().getCookies();
			for (Cookie cookie : allCookies) {
				driver.manage().addCookie(cookie);
			}
		}

		long implicitWaitTime = Long.parseLong(CONFIG
				.getProperty("implicitwait"));
		driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
		return Constants.KEYWORD_PASS;

	}

    /**
	 * Paste the Url in web browser and open the url
	 */

	public String navigate(String object, String data) {
		APP_LOGS.debug("Navigating to URL");
		try {
			driver.navigate().to(data);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + " -- Not able to navigate"+e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String clickButton(String object, String data) {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		APP_LOGS.debug("Clicking on Button");
		try {
			WebDriverWait waitObj = new WebDriverWait(driver, 40);
			//waitObj.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			waitObj.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OR.getProperty(object))));
			// waitObj.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OR.getProperty(object))));
			driver.findElement(By.xpath(OR.getProperty(object))).click();
			// driver.findElement(By.xpath(OR.getProperty(object))).click();
			// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to click on Button"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String VerifyElementNotPresent(String object, String data) {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		APP_LOGS.debug("Verify Element Present on the page");
		try {
			boolean elePresent = driver.findElements(
					By.xpath(OR.getProperty(object))).size() != 0;
			if (elePresent) {
				return Constants.KEYWORD_FAIL + "  "
						+ "Element is Present on the Page";
			} else {
				return Constants.KEYWORD_PASS + "  "
						+ "Element is not Present on the Page";
			}
		} catch (Exception e) {
			System.out.println("Enter in Exception");
			return Constants.KEYWORD_FAIL + "Exception error: "
					+ e.getMessage();
		}

	}
	
	public String vrfyEleNotClickable(String object, String data) {
        try{
			WebDriverWait waitObj = new WebDriverWait(driver, 5);
			waitObj.until(ExpectedConditions.elementToBeClickable(By.xpath(OR.getProperty(object))));
            return Constants.KEYWORD_FAIL + "  " + "Element is clickable";
        }
        catch (Exception e){
            return Constants.KEYWORD_PASS;
        }		
	}

	/**
	 *Purpose : Check that element is present on web page or not using size method
	 * Usage: 
	 * 	data - object
	 * Date: 9/7/18
	 */
	public String chkvisibilityofele(String object, String data) {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		APP_LOGS.debug("Verify the field is Active Or Not");
		try {
			if (driver.findElements(By.xpath(OR.getProperty(object))).size() > 0) {
				System.out.println("Field is present on the Page");
				return Constants.KEYWORD_FAIL + "  ->> Field  is  present ";
			} else {
				System.out.println("Field is not present on the Page");
				return Constants.KEYWORD_PASS + "  ->> Field  is not present ";

			}
		} catch (Exception e) {
			System.out.println("Enter in this");
			return Constants.KEYWORD_FAIL + " -- Not able to click on Button"
					+ e.getMessage();
		}
	}

    
    /**
 	 * This Method Press "Enter" Button of the Keyboard using Robot Class of
 	 * Selenium. 
 	 */
	public String keyentrprs(String object, String data) {
		try {
			//imitate mouse events like ENTER
			Robot robot = new Robot();
			//Occasionally, it seemed the keypress event was not process. Slow it down so keypress and keyRelease event
			//are process
			// Press Enter
			robot.keyPress(KeyEvent.VK_ENTER);
			// Release Enter
			Thread.sleep(2000);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}

	/**
 	 * This Method Press "A" Button of the Keyboard using Robot Class of
 	 * Selenium. 
 	 */
	public String PressAKeyfrmKeyboard(String object, String data) {
		try {
			//imitate mouse events like ENTER
			Robot robot = new Robot();
			//Occasionally, it seemed the keypress event was not process. Slow it down so keypress and keyRelease event
			//are process
			// Press Enter
			robot.keyPress(KeyEvent.VK_A);
			// Release Enter
			Thread.sleep(2000);
			robot.keyRelease(KeyEvent.VK_A);
			Thread.sleep(2000);
			
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}

	/**
 	 * This Method Press "Y" Button of the Keyboard using Robot Class of
 	 * Selenium. 
 	 */
	public String PressYKeyfrmKeyboard(String object, String data) {
		try {
			//imitate mouse events like ENTER
			Robot robot = new Robot();
			//Occasionally, it seemed the keypress event was not process. Slow it down so keypress and keyRelease event
			//are process
			// Press Enter
			robot.keyPress(KeyEvent.VK_Y);
			// Release Enter
			Thread.sleep(2000);
			robot.keyRelease(KeyEvent.VK_Y);
			Thread.sleep(2000);
			
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}
	
	public String entrspc(String object, String data) {
		try {
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Keys.SPACE);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}

    /**
	 * This Method Press "Enter" Button of the Keyboard using Action Class of
	 * Selenium. Because on Qmerit React js popups ClickAddNoWait method is not
	 * Working.
	 */

	public String enterreturn(String object, String data) {
		try {
			WebElement clk = driver.findElement(By.xpath(OR.getProperty(object)));
			Actions ac = new Actions(driver);
			ac.click(clk).build().perform();
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}
	
	/**
	 * Purpose: To Get the Size(Number of Values) of the Drop down : This
	 * routine get the values of dropdown using XPath and store it into variable
	 * "prognames" and after that using Size method getting the size of Dropdrop
	 */

	public String getdropdownoptions(String object, String data) {
		APP_LOGS.debug("Get the Dropdown Values" + object);
		List<WebElement> prognames = null;
		try {
			System.out.println("Data is " + data);
			// xpath of activity from closed tb
			prognames = driver.findElements(By.xpath(OR.getProperty(object)));
			System.out.println("Size is +" + prognames.size());

			if (prognames.size() > 0) {
				for (WebElement program : prognames) {
					System.out.println(program.getText().toString());
				}
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " There is no Element in Dropdown ";
		}
		return Constants.KEYWORD_PASS + "  "
				+ "Number of Items in Dropdown list  " + " \n " + " "
				+ (prognames != null ? prognames.size() : "0");
	}

	/**
	 * This Method Enter the Text in Textbox using Action Class. Because on
	 * Qmerit React js popups writeInput method is not Working.
	 */

	public String Actionentertext(String object, String data) {
		try {
			WebElement text =driver.findElement(By.xpath(OR.getProperty(object)));
			Actions ac = new Actions(driver);
			ac.sendKeys(text,data).build().perform();
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}

	
    /**
	 * This Method is used to click on Any Webelement Without Wait First we need
	 * to locate the element using Xpath on which we want to click. then After
	 * using click() method we will click on the particular webelement.
	 */
	public String ClickAddNoWait(String object, String data) {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		APP_LOGS.debug("ClickAddNoWait" + object);
		System.out.println("ClickAddNoWait: " + object);
		boolean elementClicked = false;
		try {
			WebDriverWait waittime = new WebDriverWait(driver, 3);
			WebElement element = waittime.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OR.getProperty(object)))); 
			element.click();
			elementClicked = true;
			
//			Thread.sleep(500);
//			WebElement element = driver.findElement(By.xpath(OR.getProperty(object)));
//			for (int loop = 0; loop < 25; loop++) {
//				if (element.isDisplayed() && element.isEnabled()) {
//					Thread.sleep(1000);
//					element.click();
//					elementClicked = true;
//					System.out.println("element is clicked");
//					APP_LOGS.debug("element is clicked");
//					break;
//				}
//				Thread.sleep(200);
//				element = driver.findElement(By.xpath(OR.getProperty(object)));
//			}
			if (!elementClicked) {
				System.out.println("Element is not clickable after 5 seconds");
				return Constants.KEYWORD_FAIL + "Element is not clickable after 3 seconds";
			}
		} catch (Exception e) {
			try {
				APP_LOGS.debug("retry after failing to click the element");
				Thread.sleep(3000);				
				//try to find and click it again before failing it
				driver.findElement(By.xpath(OR.getProperty(object))).click();
				APP_LOGS.debug("retry successful");
			} catch (Exception e2) {
				return Constants.KEYWORD_FAIL + " -- Retry Not able to click on Button"
						+ e2.getMessage();
			}
		}
		System.out.println("Exiting ClickAddNoWait routine");
		return Constants.KEYWORD_PASS;
	}
	
	public String assertLinkNotPresent(String object, String data) {
		try {
			driver.findElement(By.xpath(OR.getProperty(object)));
			fail("Accept Button ");
		} catch (NoSuchElementException ex) { 
			// do nothing, link is not present, assert is passed / 
			System.out.println("Enter in this");
			return Constants.KEYWORD_PASS  + "   Accept button is not present n the screen ";
		}
		return Constants.KEYWORD_FAIL + " -- Not able to perform the action";
	}

    /**
	 * Explicit Wait This Method is wait for maximum 40 secs till the visibility
	 * of the element If element get visible before 40 secs then method would
	 * perform further action, it would not wait till 40 secs
	 */

	public String Waitforelements40sec(String object, String data) {

		APP_LOGS.debug("Wait for element" + object);
		System.out.println("Wait for element: " + object);
		try {
			WebDriverWait waitObj = new WebDriverWait(driver, 40);
			waitObj.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OR.getProperty(object))));
			System.out.println(waitObj);
			//work-around for bug 19530.
			Thread.sleep(1000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}
		System.out.println("Found element: " + object);
		return Constants.KEYWORD_PASS;

	}
	
	
	/**
	 * This method Verify that Data value is not present in the list. If List is
	 * Empty it return proper message. If Data is not found in the it return a
	 * proper message
	 * Date: 4/13/2018
	 * @param object
	 * @param data
	 * @return
	 */
	public String VerifyNotInlist(String object, String data) {

		try {
			// WebDriverWait wait = new WebDriverWait(driver, 25);
			// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = '" + data + "'");
			data=data.toLowerCase();
			Thread.sleep(1000);
			List<WebElement> options = driver.findElements(By.xpath(OR
					.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +" + options.size());
			if (options != null && options.size() > 0) {
				boolean matchFound = false;
				for (WebElement opt : options) {
					System.out.println("opt is '" + (opt.getText().toLowerCase()) + "'"
							+ "data is '" + (data) + "'");
					if (opt.getText().toLowerCase().toString().contains((data))) {
						matchFound = true;
						System.out.println("Enter inside the Method");
						opt.click();
					}
				}
				if (!matchFound) {
					APP_LOGS.debug("Unable to select the element");
					System.out.println("Element is not in the List");
					return Constants.KEYWORD_PASS + "  :  "
							+ " Element "+ (data)+" not in the List" + " :: "
							+ "Expected Text is '" + (data)  + "'";
				}
			} else {
				System.out.println("List is empty");
				return Constants.KEYWORD_FAIL
						+ " No Element Found inside the list" + "   "
						+ "List is Empty";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + "Element not in the List"
					+ e.getMessage();
		}
		return Constants.KEYWORD_FAIL+ " Element "+ (data)+" present in the List" + " :: "
				+ "Expected Text is '" + (data)  + "'";
	}
	
	

	
	/**
	 * This method Verify that Data value is present in the list. If List is
	 * Empty it return proper message. If Data is not found in the it return a
	 * proper message
	 * Date: 4/13/2018
	 * @param object
	 * @param data
	 * @return
	 */
	public String VerifyInlist(String object, String data) {

		try {
			// WebDriverWait wait = new WebDriverWait(driver, 25);
			// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = '" + data + "'");
			data=data.toLowerCase();
			Thread.sleep(1000);
			List<WebElement> options = driver.findElements(By.xpath(OR
					.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +" + options.size());
			if (options != null && options.size() > 0) {
				boolean matchFound = false;
				for (WebElement opt : options) {
					System.out.println("opt is '" + (opt.getText().toLowerCase()) + "'"
							+ "data is '" + (data) + "'");
					APP_LOGS.debug("opt is '" + (opt.getText().toLowerCase()) + "'"
							+ "data is '" + (data) + "'");
					if (opt.getText().toLowerCase().toString().contains((data))) {
						matchFound = true;
					}
				}
				if (matchFound) {
					APP_LOGS.debug("Element is in the List");
					System.out.println("Element is in the List");
					return Constants.KEYWORD_PASS + "  :  "
							+ " Element "+ (data)+"  in the List" + " :: "
							+ "Expected Text is '" + (data)  + "'";
				}
			} else {
				System.out.println("List is empty");
				return Constants.KEYWORD_FAIL
						+ " No Element Found inside the list" + "   "
						+ "List is Empty";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + "Element not in the List"
					+ e.getMessage();
		}
		return Constants.KEYWORD_FAIL+ " Element "+ (data)+" not present in the List" + " :: "
				+ "Expected Text is '" + (data)  + "'";
	}
	
	public String ClicktillElementispresent(String object, String data) {
		APP_LOGS.debug("cleartxt");
		try {
			WebElement btn_Submit = driver.findElement(By.xpath(OR.getProperty(object)));
			while(this.isClickable(btn_Submit, driver)){
			{
				System.out.println("Clickable? " + btn_Submit.isEnabled());
				btn_Submit.click();
				Thread.sleep(2000);
				
			}
				
			}
			//Thread.sleep(50000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Not able to clear";
		}
		return Constants.KEYWORD_PASS;
	}
	
	public boolean isClickable(WebElement el, WebDriver driver) 
	{
		try{
			WebDriverWait wait = new WebDriverWait(driver, 3);
			wait.until(ExpectedConditions.elementToBeClickable(el));
			return true;
		}
		catch (Exception e){
			return false;
		}
	}
	/**
	 * Purpose :: To Verify the Certain Webelement is Active on the webpage
	 * Date: 4/13/2018
	 * NOTE: This routine does not work. The way coreapp presents button,
	 *       the display button is not a "button" type, hence element.isenabled 
	 *       does not work 
	 */
	//Comment out both VerifyButtonActive and VerifyButtonNotActive
	//        as they do not work. We'll have to deal with it when we run into failures
	/*
	public String VerifyButtonActive(String object, String data) {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		APP_LOGS.debug("Verify the button is Active");
		try {
			boolean isEnabled = driver.findElement(By.xpath(OR.getProperty(object))).isEnabled();
			System.out.println("element is enabled?:" + isEnabled);
			APP_LOGS.debug("element is enabled?:" + isEnabled);
			//WebElement btn1 = driver.findElement(By.xpath(OR.getProperty(object)));
			if (isEnabled) {
				System.out.println("Button is Active");
				return Constants.KEYWORD_PASS + "  ->> Button is Active ";
			} else {
				System.out.println("Button is not Active");
				return Constants.KEYWORD_FAIL
						+ "  ->> Button is not Active ";
			}
		} catch (Exception e) {
			System.out.println("Enter in this");
			return Constants.KEYWORD_FAIL + " -- Some Exception found"
					+ e.getMessage();
		}

	}
	*/
	
	/**
	 * Purpose :: To Verify the Certain Webelement is not Active on the webpage
	 * Date: 4/13/2018
	 * NOTE: THIS ROUTINE DOES NOT WORK. classes.contains("commandbutton-button--disabled")
	 * 		ALWAYS RETURNS FALSE WHETHER ELEMENT IS ENABLED OR NOT.
	 * 		Checking the isenabled property does not work with the way coreapp's button are made.
	 */
	/*
	public String VerifyButtonNotActive(String object, String data) {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		APP_LOGS.debug("Verify the button is not Active");
		try {
			WebElement btn = driver.findElement(By.xpath(OR.getProperty(object)));
			String classes = btn.getAttribute("class");
			//classes.contains("commandbutton-button--disabled") returns true for everything
			//but disabled input elements
			boolean isEnabled = classes.contains("commandbutton-button--disabled");
			System.out.println("Element is enabled?:" + isEnabled);
			APP_LOGS.debug("element is enabled?:" + isEnabled);
			//WebElement btn1 = driver.findElement(By.xpath(OR
			//		.getProperty(object)));
			if (isEnabled) {
				//btn.click();
				System.out.println("Button is Active");
				return Constants.KEYWORD_FAIL + "  ->> Button is Active ";
			} else {
				System.out.println("Button is not Active");

				return Constants.KEYWORD_PASS
						+ "  ->> Button is not Active ";
			}
		} catch (Exception e) {
			System.out.println("Enter in this");
			return Constants.KEYWORD_FAIL + " -- Some Exception found"
					+ e.getMessage();
		}

	}
    */
	/**
	 * Purpose :: This is a hack to verify whether submit button is disabled as verifybuttonactive
	 *            and verifybuttonnotactive routines are not working.
	 *            Find the submit button, attempt to perform a click action and look
	 *            for the cancel button. If Cancel button is found then it means 
	 *            the submit button was not disabled
	 * Date: 4/13/2018
	 */
	public String VerifySubmitButtonNotActive(String object, String data) {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		APP_LOGS.debug("VerifySubmitButtonNotActive");
		try {
			WebElement btn = driver.findElement(By.xpath(OR.getProperty(object)));
			String classes = btn.getAttribute("class");
			//classes.contains("commandbutton-button--disabled") returns true for everything
			//but disabled input elements
			boolean isEnabled = classes.contains("commandbutton-button--disabled");
			System.out.println("Element is enabled?:" + isEnabled);
			APP_LOGS.debug("element is enabled?:" + isEnabled);
			
			driver.findElement(By.xpath(OR.getProperty(object))).click();
			Thread.sleep(3000);
			try {
				WebElement cancelBtn = driver.findElement(By.xpath(OR.getProperty("sbmtbidcancelbtn")));
				System.out.println("Button is Active");
				return Constants.KEYWORD_FAIL + "  ->> Button is Active ";

			} catch (Exception e) {
				System.out.println("Submit Button is not Active");
				return Constants.KEYWORD_PASS
						+ "  ->> Submit Button is not Active ";
			}
		} catch (Exception e) {
			System.out.println("Enter in this");
			return Constants.KEYWORD_FAIL + " -- Some Exception found"
					+ e.getMessage();
		}
	}
	
	/**
	 * Purpose :: In Qmerit app, when button got active then color changed from
	 * grey(Not Active) to green(Active) So, here using color property we are 
	 * verifying the button is disabled.
	 * Date: 7/31/2018
	 */
	public String VerifyButtonNotActiveUsingColor(String object, String data) {
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		APP_LOGS.debug("VerifySubmitButtonNotActive");
		try {
			System.out.println("Data is " + data);
			WebElement btn = driver
					.findElement(By.xpath(OR.getProperty(object)));
			String colour = driver
					.findElement(By.xpath(OR.getProperty(object))).getCssValue(
							"background-color");
			System.out
					.println("Value of color in RGB Fomrmat " + "  " + colour);
			if (colour.equals(data)) {
				System.out.println("Button is Not Active");
				return Constants.KEYWORD_PASS + " Button is Not Active ::"
						+ colour + "  " + data;
			} else {
				btn.click();
				return Constants.KEYWORD_FAIL + " Button is Active ::" + colour;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + e.getMessage();
		}
	}
	

	
	/**
	 * Purpose :: Get the service provider Address value using xpath and 
	 * store the service provider address in a "SPAddress"variable. 
	 * It is Application based menthod specific for QMerit BMW information Page
	 * @param : object
	 * @param : data
	 * Date: 4/27/2018
	 */
	public String GetSPAddress(String object, String data) {
		APP_LOGS.debug("GetSPAddress");
		try {
			SPAddress = driver.findElement(By.xpath(OR.getProperty(object)))
					.getAttribute("value");
			System.out.println("Service Provider Address Present in Textbox ->"
					+ SPAddress);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL
					+ "- Not able get the Value of the Address";
		}
		return Constants.KEYWORD_PASS + "    "
				+ "Service Provider Address Present in Textbox ->" + SPAddress;

	}

	
	/**
	 * Purpose :: Provide Space between the words using Action class of Webdriver.
	 * 
	 */
	public String spaceaction(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.SPACE).build().perform();

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS   ;
	}
	
	/**
	 * Purpose :: Verify the Service Provider Address
	 * @param : object
	 * @param : data
	 */
	public String VerifySPAddress(String object, String data) {
		APP_LOGS.debug("VerifySPAddress");
		try {
			if (SPAddress.contains(data)) {
				System.out.println("Address Verified" + "  " + data + "  "
						+ "is present in the Address");
				return Constants.KEYWORD_PASS + "    " + data + "   "
						+ " is present in the Address";
			} else {
				System.out.println("Address not Verified");
				return Constants.KEYWORD_FAIL + "    " + data + "   "
						+ " is not present in the Address";
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + "    " + data + "   "
					+ " is not present in the Address";
		}

	}

      /**
	 * This Method is used to press "Tab" Key of Keyboard Using Action Class
	 */

	public String tabaction(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.TAB).build().perform();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS   ;
	}

    public String mouseactions(String object, String data) {
		try {
			//WebElement scrollElement = driver.findElement(By.xpath(OR.getProperty(object)));
			WebElement element = driver.findElement(By.xpath(OR.getProperty(object)));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).build().perform();
			builder.click(element).build().perform();
			//mouseOvertext.perform();   
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to scroll down to the element"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
    
    /**
	 * This Method is used for write data in input field using Action Class. 
	 * @param object
	 * @param data
	 * @return
	 */
	public String writeusingaction(String object, String data) {
		try {
			// WebElement scrollElement =
			// driver.findElement(By.xpath(OR.getProperty(object)));
			WebElement element = driver.findElement(By.xpath(OR
					.getProperty(object)));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).build().perform();
			builder.sendKeys(data).build().perform();
			// mouseOvertext.perform();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to scroll down to the element"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
    
	public String mouseActionHover(String object, String data) {
		try {
			// WebElement scrollElement =
			// driver.findElement(By.xpath(OR.getProperty(object)));
			WebElement element = driver.findElement(By.xpath(OR
					.getProperty(object)));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).build().perform();
			builder.click(element).build().perform();
			// mouseOvertext.perform();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to scroll down to the element"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
	
	/**
	 * This routine verify that added Program Name in Parent Account(Ashby6868)
	 * should be deleted and not present in Child Account(Ashby7070) of added
	 * program List.
	 * @param object
	 * @param data
	 * @return
	 */
	public String VerifyDeletedProgname(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			System.out.println("Data is " + data);
			// xpath of activity from closed tb

			List<WebElement> prognames = driver.findElements(By.xpath(OR
					.getProperty(object)));
			System.out.println("Size is +" + prognames.size());
			if (prognames != null && prognames.size() > 0) {
				boolean matchFound = false;
				for (WebElement program : prognames) {
					System.out.println(program.getText().toString());
					if (program.getText().toString().equalsIgnoreCase(data)) {
						matchFound = true;
						System.out.println("Enter inside the Method");
						System.out.println("Verified");
						break;
					}
				}
				if (matchFound)
					return Constants.KEYWORD_FAIL + "Program Name  " + data
							+ " " + " is Present in the List";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + "Exception trigger in VerifyDeletedProgname routine";
		}
		return Constants.KEYWORD_PASS + " " + "Verified " + "::"
				+ " Element is not added in the List Expected Text is '" + "  "
				+ (data) + "'";
}

      /**
	 * This Method Press "Down Arrow" Button of the Keyboard using Robot Class
	 * of Selenium.
	 * 
	 */

	public String movedownthrurobot(String object, String data) {
		try {
			// WebElement scrollElement =
			// driver.findElement(By.xpath(OR.getProperty(object)));
			Robot robo = new Robot();
			robo.keyPress(KeyEvent.VK_DOWN);
			robo.keyRelease(KeyEvent.VK_DOWN);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable Down Arrow"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	} 
	
	
	
	/*
	 * Purpose: hover mouse over object and compare the pop-up tooltip with
	 * expected tooltip. requiredfor is defined in or.properties file. TODO:
	 * look for a better way to pass in 2nd object instead of hardcoded the
	 * property requiredfor Usage : This routine is specific to Manage
	 * Requirements.
	 */
	public String mousehoverRequiredfor(String object, String data) {
		try {
			// WebElement scrollElement =
			// driver.findElement(By.xpath(OR.getProperty(object)));
			WebElement element = driver.findElement(By.xpath(OR
					.getProperty(object)));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).build().perform();

			WebElement toolTipElement = driver.findElement(By.cssSelector(OR
					.getProperty("requiredfor")));
			Thread.sleep(2000);
			// To get the tool tip text and compares
			String toolTipText = toolTipElement.getText();
			System.out.println(toolTipText);
			if (toolTipText.contains(data)) {
				System.out.println("verified : " + "Expected -> " + data
						+ "    " + "Actual -> " + toolTipText);
			} else {
				System.out.println("Not verified : " + "Expected -> " + data
						+ "    " + "Actual -> " + toolTipText);
				return Constants.KEYWORD_FAIL
						+ " Tooltip text not found or text not match ";
			}
			// builder.click(element).build().perform();
			// mouseOvertext.perform();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to get tooltip text"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	/**
	 * This Method Press " Enter " Button of the Keyboard using Robot Class of
	 * Selenium.
	 * 
	 */
	public String EnterPressThruRobot(String object, String data) {
		try {
			// WebElement scrollElement =
			// driver.findElement(By.xpath(OR.getProperty(object)));
			Robot robo = new Robot();
			robo.keyPress(KeyEvent.VK_ENTER);
			robo.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- Unable to press Enter Button of keyboard"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS + "  "
				+ "Enter Button of Keyboard pressed Sucessfully";
	}

	public String mouseactionsclick(String object, String data) {
		try {
			WebElement element = driver.findElement(By.xpath(OR
					.getProperty(object)));
			Actions builder = new Actions(driver);
			builder.moveToElement(element).build().perform();
			Thread.sleep(2000);
			builder.click(element).build().perform();
			System.out.println("mouseaction clicked");
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to click to the element" + e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String selectmulbutton(String object, String data) {
		APP_LOGS.debug("Selecting a radio button");
		try {
			//List<WebElement> radiobutton = driver.findElements(By.xpath(OR.getProperty(object)));
			List<WebElement> element=  driver.findElements(By.xpath(".//*[@class='question-row']"));
			System.out.println("Total element is " + element.size());
			for (int i = 1; i < element.size(); i++) {
				Thread.sleep(1000);
				element.get(i).findElement(By.xpath(".//*[@class='yesno no']")).click();
				// radiobutton = driver.findElements(By.xpath(OR.getProperty(object)));
				//System.out.println(radiobutton.get(i).getAttribute("value"));
				// select your radio and click to go to next page
				//radiobutton.get(i).click();
			} 
		} catch (Exception e) {
				System.out.println(e.getMessage());
				return Constants.KEYWORD_FAIL + "- Not able to find radio button";
		}
		return Constants.KEYWORD_PASS;
	}

	public String Waitforelements100sec(String object, String data) {

		APP_LOGS.debug("Wait1 (100 secs for element)");
		try {
			// Thread.sleep(50000);
			WebDriverWait waitObj = new WebDriverWait(driver, 100);
			waitObj.until(ExpectedConditions.visibilityOfElementLocated(By
					.xpath(OR.getProperty(object))));
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String Wait10secs(String object, String data) {

		APP_LOGS.debug("Wait 10 secs");
		try {
			Thread.sleep(10000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

      /**
	 * This method is used for force wait to webdriver to see some activity on
	 * the webpage.
	 */
	public String Wait3secs(String object, String data) {

		APP_LOGS.debug("Wait3secs");
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String Wait2secs(String object, String data) {

		APP_LOGS.debug("Wait3secs");
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String refresh(String object, String data) {

		APP_LOGS.debug("refresh the page");
		try {
			driver.navigate().refresh();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String randomemail(String object, String data) {

		APP_LOGS.debug("randomemail");
		try {

			Random rad = new Random();
			String randomemail=	"qmerit10+consumer511"+"563"+rad.nextInt(100)+"@gmail.com";
			System.out.print("Random email id is -> " + randomemail); 
			//driver.findElement(By.xpath("//input[@name='email']")).sendKeys(randomemail);
			// driver.findElement(By.xpath("//input[@name='confirmEmail']")).sendKeys(randomemail);

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}

		return Constants.KEYWORD_PASS;
	}//String randomemail;

	public String Wait5secs(String object, String data) {
		APP_LOGS.debug("Wait3 (5 seconds)");
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
	
	public String Wait8secs(String object, String data) {
		APP_LOGS.debug("Wait3 (8 seconds)");
		try {
			Thread.sleep(8000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to wait"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
	
	public String scrollPage(String object, String data) {

		APP_LOGS.debug("scrollPage");
		try {
			//selenium.getEval("scrollBy(0, 250)");
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,250)", "");
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to scroll"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String switchWindow(String object, String data){

		APP_LOGS.debug("Switching Windows");
		try {
			//driver.switchTo().window(By.)
			System.out.println("Switching Windows");
			String winHandleBefore = driver.getWindowHandle();
			// store all the windows 
			Set<String> handle= driver.getWindowHandles();
			// iterate over window handles
			for (String mywindows : handle) {
				// store window title
				String myTitle = driver.switchTo().window(mywindows).getTitle();
				// now apply the condition - moving to the window with blank title
				if(myTitle.equals("Parent Window")){
					// perform some action - as here m openning a new url
					// driver.get("http://docs.seleniumhq.org/download/");
					System.out.println("Hty");
				} else {
					driver.switchTo().window(winHandleBefore);
				}
			}
		}catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to Switch"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String switchbrowser(String object, String data){

		APP_LOGS.debug("Switching Windows");

		try {
			System.out.println("Enter in the method");
			ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
			System.out.println("Size is "+tabs2.size());
			driver.switchTo().window(tabs2.get(1));
			System.out.println(driver.switchTo().window(tabs2.get(1)));
			Thread.sleep(9000);
			//   driver.close();
			//driver.switchTo().window(tabs2.get(0));
			// driver.switchTo().defaultContent();
			// System.out.println(driver.switchTo().window(tabs2.get(0)));
			//driver.switchTo().window(By.)
		}catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to Switch"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String switchontewtab(String object, String data){

		APP_LOGS.debug("Switching Windows");

		try {
			//driver.switchTo().window(By.)
			System.out.println("Switching Windows");
			String winHandleBefore = driver.getWindowHandle();
			// store all the windows 
			Set<String> handle= driver.getWindowHandles();
			// iterate over window handles
			for (String mywindows : handle) {
				// store window title
				String myTitle = driver.switchTo().window(mywindows).getTitle();
				// now apply the condition - moving to the window with blank title
				if(myTitle.equals("Parent Window")){
					// perform some action - as here m openning a new url
					// driver.get("http://docs.seleniumhq.org/download/");
					System.out.println("Hty");
				} else {
					driver.switchTo().window(winHandleBefore);
				}
			}
		}catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to Switch"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

    /**
	 * This Method is used to Handle the Selenium Actions in Newly open browser
	 * Tab
	 */
	public String switchonnewtab(String object, String data){

		APP_LOGS.debug("switchonnewtab");
		try {
			String url1 = driver.getCurrentUrl();
			System.out.println(url1);
			ArrayList <String>tabs = new ArrayList<String> (driver.getWindowHandles());
			System.out.println(tabs.size());
			driver.switchTo().window(tabs.get(1));
			String url2 = driver.getCurrentUrl();
			System.out.println(url2);
		}catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to Switch"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
	
	public String switchonscndtab(String object, String data){

		APP_LOGS.debug("switchonscndtab");
		try {
			String url1 = driver.getCurrentUrl();
			System.out.println(url1);
			ArrayList <String>tabs = new ArrayList<String> (driver.getWindowHandles());
			System.out.println(tabs.size());
			driver.switchTo().window(tabs.get(2)).getCurrentUrl();
			Thread.sleep(3000);
			System.out.println(driver.switchTo().window(tabs.get(2)).getCurrentUrl());
		}catch (Exception e) {

			return Constants.KEYWORD_FAIL + " -- Not able to Switch"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

    /**
	 * This Method is used to Handle the Selenium Actions in previously browser
	 * Tab
	 */
	public String switchonoldtab(String object, String data){

		APP_LOGS.debug("switchonoldtab");

		try {
			String url1 = driver.getCurrentUrl();
			System.out.println(url1);
			ArrayList <String>tabs = new ArrayList<String> (driver.getWindowHandles());
			System.out.println(tabs.size());
			driver.switchTo().window(tabs.get(1));
		}catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to Switch"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	    /**
		 * This Method is used to Handle the Selenium Actions in previously browser
		 * Tab
		 */
		public String switchToOriginalTab(String object, String data){
			APP_LOGS.debug("switchToOriginalTab");
			try {
				String url1 = driver.getCurrentUrl();
				System.out.println(url1);
				ArrayList <String>tabs = new ArrayList<String> (driver.getWindowHandles());
				System.out.println(tabs.size());
				driver.switchTo().window(tabs.get(0));
			}catch (Exception e) {
				return Constants.KEYWORD_FAIL + " -- Not able to Switch"
						+ e.getMessage();
			}
			return Constants.KEYWORD_PASS;
		}
	
	public String selectRadio(String object, String data) {
		APP_LOGS.debug("Selecting a radio button");
		try {
			String temp[] = object.split(Constants.DATA_SPLIT);
			driver.findElement(By.xpath(OR.getProperty(temp[0]))).click();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + "- Not able to find radio button";
		}

		return Constants.KEYWORD_PASS;

	}
	long l3;
	
	/*	public String pdfmatch(String object, String data) {
        try {
            PDDocument pd ;

            //pd =driver.getCurrentUrl();
            System.out.println("Enter in pdf");
            //String urlStr = driver.getCurrentUrl();
            String urlStr  = driver.getCurrentUrl();
            System.out.println(urlStr);

            String fileName = urlStr.substring(urlStr.lastIndexOf('/')+1, urlStr.length());
            String fileNameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
            String fileExtension = urlStr.substring(urlStr.lastIndexOf("."));

            System.out.println(fileName);
            System.out.println(fileNameWithoutExtension);
            System.out.println(fileExtension);

            pd =PDDocument.load(new File(fileName+fileNameWithoutExtension+fileExtension));
            System.out.println("Total Number of Pages" +pd.getNumberOfPages());
            PDFTextStripper pdf = new PDFTextStripper();
            String ele = pdf.getText(pd);
            //System.out.println(ele);
            for(int i=1;i<=1;i++)
            {
        //        String evai=ele.replace("Total Installment ", "/ ");
                //ele.replace("Total Installment",  "/ ");
                  String ar[]=ele.replace("Total Installment Premium",  "= ").split("=");

                  String ar1[]=ar[1].split("\\r?\\n");
                  String ar2 = ar1[2].replace("(rounded off to nearest Rupee)", " ");
                  System.out.println("Value is "+ar2);
                  l3 = Long.parseLong(ar2.replaceAll(",", ""));
                    System.out.println("Value of 1 is stored in  l3 : " + l3);
            }
        } catch (Exception e) {
            return Constants.KEYWORD_FAIL + " -- Unable to store value"
                    + e.getMessage();
        }

        return Constants.KEYWORD_PASS;
    }
	 */
	public String scrollPagedowdown(String object, String data) {

		APP_LOGS.debug("scrollPagedowdown");
		try {
			//selenium.getEval("scrollBy(0, 250)");

			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,450)", "");

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to scroll"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String scrollPagedownup(String object, String data) {

		APP_LOGS.debug("scrollPagedownup");
		try {
			//selenium.getEval("scrollBy(0, 250)");

			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,-250)", "");

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to scroll"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String Alert(String object, String data) {
		APP_LOGS.debug("Pop Up Message Verify");
		try {

			/*StringBuffer verificationErrors = new StringBuffer(); 
			String verificationErrorString = verificationErrors.toString(); 
			if (!"".equals(verificationErrorString)) { 
				fail(verificationErrorString);
			System.out.println("verificationErrorString");
			} */

			String parentWindowHandler = driver.getWindowHandle(); // Store your parent window
			String subWindowHandler = null;

			Set<String> handles = driver.getWindowHandles(); // get all window handles
			Iterator<String> iterator = handles.iterator();
			while (iterator.hasNext()){
				subWindowHandler = iterator.next();
			}
			driver.switchTo().window(subWindowHandler); // switch to popup window
			driver.findElement(By.xpath(OR.getProperty(object))).click();                                          // perform operations on popup

			//driver.switchTo().window(parentWindowHandler);

			//Alert alert = driver.switchTo().alert();
			//	String msg = alert.getText();
			//	System.out.println(alert.getText());
			//Assert.assertEquals(msg, data);
			//alert.accept();
			//alert.dismiss();
		} catch (Exception e) {

			return Constants.KEYWORD_FAIL
					+ "- Verification message does not match.";
			//   Assert.fail();
		}

		return Constants.KEYWORD_PASS;

	}
	
	/**
	 * This Method is used to Handle the Iframe in Selenium 
	 * In Selenium Iframe is treated as different web element 
	 * So perform any activity inside the Iframe we need to
	 * Switch first inside the Iframe.
	 */
	
	public String SwitchtoIframe(String object, String data) {

		APP_LOGS.debug("Switching Windows");
		try {
			String url1 = driver.getCurrentUrl();
			System.out.println(url1);
			driver.switchTo().frame(driver.findElement(By.id("typeform-full")));
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to Switch"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String AlertMessage(String object, String data) {
		APP_LOGS.debug("Pop Up Message Verify");
		try {
			String verificationErrors="";
			String verificationErrorString = verificationErrors.toString(); 
			if (!"".equals(verificationErrorString)) { 
				fail(verificationErrorString);
				System.out.println("verificationErrorString");
			} 
			Alert alert = driver.switchTo().alert();
			//	String msg = alert.getText();
			//	System.out.println(alert.getText());
			//Assert.assertEquals(msg, data);
			//alert.accept();
			alert.dismiss();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ "- Verification message does not match.";
			//   Assert.fail();
		}
		return Constants.KEYWORD_PASS;
	}

	public String selectList(String object, String data) {
		APP_LOGS.debug("Selecting from list");
		try {
			if (!data.equals(Constants.RANDOM_VALUE)) {
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(
						data);
			} else {
				// logic to find a random value in list
				WebElement droplist = driver.findElement(By.xpath(OR
						.getProperty(object)));
				List<WebElement> droplist_cotents = droplist.findElements(By
						.tagName("option"));
				Random num = new Random();
				int index = num.nextInt(droplist_cotents.size());
				String selectedVal = droplist_cotents.get(index).getText();

				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(
						selectedVal);
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " - Could not select from list. "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String datePicker(String object, String data) {
		APP_LOGS.debug("Select the date from datepicker");
		try {
			WebElement fromDate = driver.findElement(By.xpath(OR
					.getProperty(object)));
			WebElement table = driver.findElement(By
					.xpath(object));
			List<WebElement> rows = table.findElements(By.tagName("tr"));
			java.util.Iterator<WebElement> i = rows.iterator();
			while (i.hasNext()) {
				WebElement row = i.next();
				System.out.println(row.getText());
			}

			// driver.findElement(By.xpath(OR.getProperty(object))).selectByVisibleText(data);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String selectDate(String object, String data) {
		APP_LOGS.debug("Select the date from datepicker");
		try {
			WebElement dateWidget = driver.findElement(By.id(object));
			List<WebElement> rows=dateWidget.findElements(By.tagName("tr"));
			List<WebElement> columns=dateWidget.findElements(By.tagName("td"));
			for (WebElement cell: columns){
				//Select 13th Date 
				if (cell.getText().equals(data)){
					cell.findElement(By.linkText(data)).click();
					break;
				}
				System.out.println(data);
			}
		}catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String getval(String object, String data) {
		APP_LOGS.debug("n");
		try {

			WebElement ele = driver.findElement(By.xpath(OR.getProperty(object)));
			String ele2= ele.getAttribute("value");
			System.out.println(ele2);
			ele.clear();
			if(ele2.equals(200000)){
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(ele2);
				//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			}
			else{
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(data);
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + "- Not able to find radio button";

		}

		return Constants.KEYWORD_PASS;

	}

	public String gettextact(String object, String data) {
		APP_LOGS.debug("n");
		try {
			OpenActText = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			System.out.println("Activity Number is ->"  +OpenActText);
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + "- Not able get actvity number";
		}
		return Constants.KEYWORD_PASS +"    "+" Activity Number is -> "  +OpenActText ;

	}

	/**
	 * Gets appointment time (from SP) and stores it for checking later
	 * @param object
	 * @param data
	 * @return
	 */
	public String gettexttme(String object, String data) {
		APP_LOGS.debug("n");
		try {
			AppTme = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			System.out.println("Appointment Time is ->"  +AppTme);
		}catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + "- Not able to get the time";
		}

		return Constants.KEYWORD_PASS + "    "+" Appointment Time given by Service Provider -> "  +AppTme;
    }
	
	/**
	 * Gets appointment time Customer Gmail and stores it for checking later
	 * @param object
	 * @param data
	 * @return
	 */
	public String getApptimefromGmail(String object, String data) {
		APP_LOGS.debug("n");
		try {
			String GmailAppTme = driver.findElement(
					By.xpath(OR.getProperty(object))).getText();
			time = GmailAppTme.substring(0, 10);
			System.out.println("Appointment Time is ->" + time);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + "- Not able to get the time";
		}

		return Constants.KEYWORD_PASS + "    "
				+ " Appointment Time given by Service Provider -> " + time;
	}

	public String dropDownList(String object, String data) {
		APP_LOGS.debug("Select the dropdown list");
		try {
			// driver.switchTo().frame("titlebar");
			// WebElement dropDown =
			// driver.findElement(By.xpath(OR.getProperty(object)));
			Select value = new Select(driver.findElement(By.xpath(OR
					.getProperty(object))));
			List<WebElement> size = value.getOptions();
			//System.out.println("size is" + size);

			value.selectByVisibleText(data);
			// value.selectByValue(data);
			// value.selectByIndex(2);
			//Thread.sleep(3000);

			// driver.findElement(By.xpath(OR.getProperty(object))).selectByVisibleText(data);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
	
	public String selectfromlist(String object, String data) {

		try {
			//WebDriverWait wait = new WebDriverWait(driver, 25);
			//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = '" + data+"'");
			Thread.sleep(2000);
			List<WebElement> options =   driver.findElements(By.xpath(OR.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +"+options.size());

			if(options!=null && options.size()>0){
				boolean matchFound = false;
				for (WebElement opt : options) {
					System.out.println("opt is '"+(opt.getText())+"'"+ "data is '"+(data)+"'");
					if (opt.getText().toString().equals((data))) {
						matchFound=true;
						System.out.println("Enter inside the Method");
						opt.click();
						break;
					}
				}
				if (!matchFound) {
					APP_LOGS.debug("Unable to select the element");
					System.out.println("Element is not in the List");
					return Constants.KEYWORD_FAIL + "  :  "+" Element not in the List" +"  " + "Expected Text is '"+(data)+"'";					
				}
			} else {
				System.out.println("List is empty");
				return Constants.KEYWORD_FAIL + " No Element Found inside the list"+"   "+"List is Empty";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + "Element not in the List"	+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
	
	public String VrfyAddedMngReq(String object, String data) {

		try {
			//WebDriverWait wait = new WebDriverWait(driver, 25);
			//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = '" + data+"'");
			Thread.sleep(2000);
			List<WebElement> options =   driver.findElements(By.xpath(OR.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +"+options.size());
			if (options!=null && options.size()>0) {
				boolean matchFound = false;
				for (WebElement opt : options) {
					System.out.println("opt is '"+(opt.getText())+"'"+ "data is '"+(CONFIG.getProperty("Requirement Name"))+"'");
					if (opt.getText().toString().equals((CONFIG.getProperty("Requirement Name")))) {
						matchFound=true;
						System.out.println("Enter inside the Method");
						opt.click();
					}
				}
				if (!matchFound) {
					APP_LOGS.debug("Unable to select the element");
					System.out.println("Element is not in the List");
					return Constants.KEYWORD_FAIL + "  :  "+" Element not in the List" +"  " + "Expected Text is '"+(data)+"'";					
				}
			} else {
				System.out.println("List is empty");
				return Constants.KEYWORD_FAIL + " No Element Found inside the list"+"   "+"List is Empty";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + "Element not in the List"	+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String selectpartiallytextfromlist(String object, String data) {

		try {
			//WebDriverWait wait = new WebDriverWait(driver, 25);
			//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = '" + data+"'");
			Thread.sleep(2000);
			List<WebElement> options =   driver.findElements(By.xpath(OR.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +"+options.size());
			if (options!=null && options.size()>0) {
				boolean matchFound = false;
				for (WebElement opt : options) {
					System.out.println("opt is '"+(opt.getText())+"'"+ "data is '"+(data)+"'");
					if (opt.getText().toString().contains((data)) || opt.getText().toString().contains((data.toLowerCase()))
							|| opt.getText().toString().contains((data.toUpperCase()))) {
						matchFound=true;
						System.out.println("Enter inside the Method");
						opt.click();
						break;
					}
				}
				if (!matchFound) {
					APP_LOGS.debug("Unable to select the element");
					System.out.println("Element is not in the List");
					return Constants.KEYWORD_FAIL + "  :  "+" Element not in the List" +"  " + "Expected Text is '"+(data)+"'";					
				}
			} else {
				System.out.println("List is empty");
				return Constants.KEYWORD_FAIL + " No Element Found inside the list"+"   "+"List is Empty";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + "Element not in the List"	+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	/**
	 * This Method select Value from the List using Action Class Because
	 * ClickAddNoWait Method is not working
	 */
	public String selectfromListusingaction(String object, String data) {
		try {
			// WebDriverWait wait = new WebDriverWait(driver, 25);
			// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = " + data);
			// Thread.sleep(2000);
			List<WebElement> options = driver.findElements(By.xpath(OR
					.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +" + options.size());
			for (WebElement opt : options) {
				System.out.println(opt.getText());
				if (opt.getText().contains(data)) {
					System.out.println("Come dude");
					Actions ac = new Actions(driver);
					ac.click(opt).build().perform();
					// driver.manage().timeouts().implicitlyWait(5,
					// TimeUnit.SECONDS);
					// Thread.sleep(500);
				} else {
					APP_LOGS.debug("Unable to select the element");
				}
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;

	}

	public String verifyprognamefor_10521_10522_Manage(String object,
			String data) {
		try {
			// WebDriverWait wait = new WebDriverWait(driver, 25);
			// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = " + data);
			Thread.sleep(2000);
			List<WebElement> options = driver.findElements(By.xpath(OR
					.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +" + options.size());
			for (WebElement opt : options) {
				System.out.println(opt.getText());
				if (opt.getText().equalsIgnoreCase(data)) {
					System.out.println("Come inside the method");
					opt.click();
				} else {
					APP_LOGS.debug("Unable to Find the element");
					System.out.println("Element is not in the List");
				}
			}
			return Constants.KEYWORD_PASS + " -> Program name" + " " + data
					+ " "
					+ "not in List or Program Name Deleted Successfully  ";
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
	}

	public String vrfyprognme(String object, String data) {

		try {
			//WebDriverWait wait = new WebDriverWait(driver, 25);
			//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = " + data);
			Thread.sleep(2000);
			List<WebElement> options =   driver.findElements(By.xpath(OR.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +"+options.size());
			for (WebElement opt : options) {
				System.out.println(opt.getText());
				if (opt.getText().contains(data)) {
					System.out.println("Come enter");
					opt.click();

					//Thread.sleep(500);
				} else {
					APP_LOGS.debug("Unable to select the element");
				}
			}  
			return Constants.KEYWORD_PASS + " -> Program Name Deleted Successfully ";
		}catch (Exception e){
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
	}
	
	/*Purpose: This method is used for Verify the Activity muber present in
	 * in the List in Service Provider Account
	 */
	public String verifyActivityNumber(String object, String data) {

		try {
			//WebDriverWait wait = new WebDriverWait(driver, 25);
			//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(OR.getProperty(object)))));
			System.out.println("Data is now = " + data);
			Thread.sleep(2000);
			List<WebElement> options = driver.findElements(By.xpath(OR.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is +"+options.size());
			for (WebElement opt : options) {
				System.out.println(opt.getText());
				if (opt.getText().contains(data)) {
					System.out.println("Activity #: " + data + " found.");
					opt.click();
					break;
					//Thread.sleep(500);
				} else {
					APP_LOGS.debug("activity number not matched");
				}
			}  
			return Constants.KEYWORD_PASS + " -> Activity Number Found ";
		}catch (Exception e){
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
	}

	public String verifyText(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		String actual;
		if (data == "") {
			return Constants.KEYWORD_FAIL + " Expected text is empty:: "
					+ "Expected = " + data;			
		}
		try {
			//WebDriverWait waitObj = new WebDriverWait(driver, 10);
			//waitObj.until(ExpectedConditions.visibilityOf(driver.findElement(By
			//		.xpath(OR.getProperty(object)))));
			actual = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			// System.out.println(expected);
			// confirmation=driver.findElement(By.xpath(OR.getProperty("confirmation"))).getText();
			if (actual.contains(data)) {
				System.out.println("verified" + data);
			} else {
				APP_LOGS.debug(Constants.KEYWORD_FAIL + " Text Not Verified:: "
						+ "Expected = " + data + " Actual = " + actual);				
				return Constants.KEYWORD_FAIL + " Text Not Verified:: "
						+ "Expected = " + data + " Actual = " + actual;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
		APP_LOGS.debug(Constants.KEYWORD_PASS + " Text Verified Sucessfully :: "
				+ "Expected = " + data + " Actual = " + actual);				
		return Constants.KEYWORD_PASS + " Text Verified Sucessfully :: "
				+ "Expected = " + data + " Actual = " + actual;
	}

	public String verifyNoteAfterOverride(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		String actual;
		try {
			actual = driver.findElement(By.xpath(OR.getProperty(object)))
					.getText().toString();
			System.out.println("Actual text is :" + actual);
			if (actual.equals(data)) {
				System.out.println("verified" + data);
			} else {
				return Constants.KEYWORD_FAIL + " Notes Not Matched ";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Notes Not Matched "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS + " " + " Actual Notes : " + actual
				+ "  " + "Expected Notes : " + data;
	}

	/**
	 * Purpose: TO Verify the Partial Strings Usage : In This routine Actual Data
	 * comes from xpath from OR properties file and Expected result comes from
	 * Excel sheet If Actual and Expected Value Get Matched the Message
	 * displayed in Report With Expected and Actual Data
	 */
	public String vrfystringtext(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		String actual;
		
		if (data == "") {
			return Constants.KEYWORD_FAIL + " Expected data is empty : "
					+ "Expected -> " + data;
		}
		try {
			System.out.println("Data is -> " + data);
			actual = driver.findElement(By.xpath(OR.getProperty(object)))
					.getText();
			System.out.println("Expected -> " + data + "    "
					+ "Actual -> " + actual);
			if (actual.contains(data)) {
				System.out.println("verified : " + "Expected -> " + data
						+ "    " + "Actual -> " + actual);
			} else {
				return Constants.KEYWORD_FAIL + "Not verified : "
						+ "Expected -> " + data + "    " + "Actual -> "
						+ actual;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + data
					+ " object not found; Exception:" + e.getMessage();
		}
		return Constants.KEYWORD_PASS + "  " + "Text Verified Sucessfully :: "
				+ "Expected = " + data + "Actual = " + actual;
	}

	/**
	 * Purpose: TO Verify the Partial Strings Usage but ignore case. similar to vrfystringext 
	 * In This routine Actual Data
	 * comes from xpath from OR properties file and Expected result comes from
	 * Excel sheet If Actual and Expected Value Get Matched the Message
	 * displayed in Report With Expected and Actual Data
	 */
	public String vrfystringignorecase(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			expected = data.toLowerCase();
			System.out.println("Data is -> " + data);
			actual = driver.findElement(By.xpath(OR.getProperty(object)))
					.getText().toLowerCase();
			System.out.println("Expected -> " + expected + "    "
					+ "Actual -> " + actual);
			if (actual.contains(expected)) {
				System.out.println("verified : " + "Expected -> " + expected
						+ "    " + "Actual -> " + actual);
			} else {
				return Constants.KEYWORD_FAIL + "Not verified : "
						+ "Expected -> " + expected + "    " + "Actual -> "
						+ actual;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + data
					+ " object not found; Exception:" + e.getMessage();
		}
		return Constants.KEYWORD_PASS + "  " + "Text Verified Sucessfully :: "
				+ "Expected = " + expected + "Actual = " + actual;
	}

	/**
	 * Purpose: TO Verify the Exact Strings Usage : In This routine Actual Data
	 * comes from xpath from OR properties file and Expected result comes from
	 * Excel sheet If Actual and Expected Value Get Matched the Message
	 * displayed in Report With Expected and Actual Data
	 */
	public String vrfyexacttext(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		String actual;
		try {
			System.out.println("Data is -> " + data);
			actual = driver.findElement(By.xpath(OR.getProperty(object)))
					.getText();
			System.out.println("Actual Data is ->" + actual);
			if (actual.equalsIgnoreCase(data)) {
				System.out.println("verified : " + "Expected -> " + data
						+ "    " + "Actual -> " + actual);
			} else {
				System.out.println("Object not found");
				return Constants.KEYWORD_FAIL + " Not verified : "
						+ "Expected -> " + data + "    " + "Actual -> "
						+ actual;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + data + " object not found";
		}
		return Constants.KEYWORD_PASS + "  "
				+ "  Text Verified Sucessfully :: " + "  Expected = "
				+ data + "::" + "  Actual = " + actual;
	}

	/**
	 * Purpose: TO Verify the Email Id of the Customer
	 *  Usage : In This routine Actual Data comes from 
	 *  xpath from OR properties file and Expected result
	 *  comes from genAcct method which is used to generate 
	 *  Random Email ID If Actual and Expected Value
	 *  Get Matched the Messagedisplayed in Report With Expected and Actual Data
	 */
	public String vrfyEmailId(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {			
			actual = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			System.out.println("Actual Data is ->" + actual);
			if (CONFIG.getProperty("CMR_acct").equals(actual)) {
				System.out.println("verified : " +  "Actual -> " + actual);
			} else {
				System.out.println("Object not found");
				return Constants.KEYWORD_FAIL ;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + actual + " object not found";
		}
		return Constants.KEYWORD_PASS ;
	}
	
	/**
	 * Purpose: TO Verify that Particular Message is not present on Webpage.
	 */
	public String vrfyinCaseofTextnotFound(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		String actual;
		try {
			System.out.println("Data is -> " + data);
			actual = driver.findElement(By.xpath(OR.getProperty(object)))
					.getText();
			System.out.println("Actual Data is ->" + actual);
			if (actual.equals(data)) {
				System.out.println("verified : " + "Expected -> " + data
						+ "    " + "Actual -> " + actual);
			} else {
				System.out.println("Object not found");
				return Constants.KEYWORD_PASS + " Not verified : "
						+ "Expected -> " + data + "    " + "Actual -> "
						+ actual;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_PASS + "  ::  "
					+ " Required Message not found";
		}
		return Constants.KEYWORD_FAIL + "  "
				+ "  Text Verified Sucessfully :: " + "  Expected = "
				+ data + "::" + "  Actual = " + actual;
	}
	
	public String vrfyNotes(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		String actual = "";
		try {

			actual = driver.findElement(By.xpath(OR.getProperty(object)))
					.getText();
			System.out.println("Actual Data is ->" + actual);
			System.out.println("Data is -> " + data);
			if (actual.equals(expected)) {
				System.out.println("verified : " + "Expected -> " + data
						+ "    " + "Actual -> " + actual);
				return Constants.KEYWORD_FAIL + "  "
				+ "  Text Verified Sucessfully :: " + "  Expected = "
				+ data + "::" + "  Actual = " + actual;
			} else {
				System.out.println("Object not found");
				return Constants.KEYWORD_PASS + "  : " + "Notes Not Found";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Notes Not Found : "
					+ "Expected -> " + data + "    " + "Actual -> "
					+ actual + "" + e.getMessage();
		}
		

	}

	public String verifycmpnytxt(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			actual = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			/*
			* the statement "expected = data1" below is replaced by "expected = data".
			* data1 is a module level variable, which is assigned the value of "Ashby7070 &".
			* Should not be hard coding anything. Additionally, the routine uses data in all return and print statements.
			* I checked all the approved tests so far (10618-10620, 12057, 12060, and 10815) and none of the tests 
			* calls this routine.
			*/
			//expected = data1;
			expected = data;
			if (actual.contains(expected)) {
				System.out.println("verified : " + expected);
			} else {
				return Constants.KEYWORD_FAIL + " Object not found ";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + data + " Object not found "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS + "  " + "Text Verified Sucessfully :: " + "Expected = " + expected + "Actual = " +actual;

	}

	/**
	 * This routine verify the closed activity number is in the closed activity tab.
	 * This assumes the expected closed activity number is the first one on the list.
	 * This assumption will not be valid if multiple users are using the account.
	 * @param object
	 * @param data
	 * @return
	 */
	public String verifyactivity(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			//xpath of activity from closed tb
			CloseActText = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			System.out.println("Activity in closed tab :" +CloseActText);
			if (CloseActText.equals(OpenActText)) {
				System.out.println("Activity number in Open Tab  - " + OpenActText + "Activity number in Closed Tab - " +CloseActText);
			} else {
				return Constants.KEYWORD_FAIL + " Activity number " + OpenActText + " is not found (not first item)";				
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Object not found "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS+ "    " + "Activity Verify Sucessfully" + "     " + "Activity number in Open Tab -> " +"  "+OpenActText + "/n" +"Activity number in Closed Tab -> "+" " +CloseActText ;

	}

    public String verifyprogname(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			System.out.println("Data is " + data);
			// xpath of activity from closed tb

			List<WebElement> prognames = driver.findElements(By.xpath(OR
					.getProperty(object)));
			System.out.println("Size is +" + prognames.size());
			if (prognames != null && prognames.size() > 0) {
				boolean matchFound = false;
				for (WebElement program : prognames) {
					System.out.println(program.getText().toString());
					if (program.getText().toString().equalsIgnoreCase(data)) {
						matchFound = true;
						System.out.println("Enter inside the Method");
						System.out.println("Verified");
						break;
					}
				}
				if (matchFound)
					return Constants.KEYWORD_PASS + " "
							+ "Verified Program Name is  " + data;
			}
		} catch (Exception e) {
		}
		return Constants.KEYWORD_FAIL
				+ " : Element is not added in the List Expected Text is '"
				+ (data) + "'";
	}

/**
	 * Purpose: Verify on login of SP, Manage option should not present in
	 * MenuList Take all value of Menu in List using Xpath and compare each
	 * element of list with "Manage" text using Loop and also print all value of
	 * list in console
	 */
	public String verifymanagelink(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			System.out.println("Data is " + data);
			// xpath of activity from closed tb

			List<WebElement> prognames = driver.findElements(By.xpath(OR
					.getProperty(object)));
			System.out.println("Size is +" + prognames.size());
			if (prognames != null && prognames.size() > 0) {
				boolean matchFound = false;
				for (WebElement program : prognames) {
					System.out.println(program.getText().toString());
					if (program.getText().toString().equalsIgnoreCase(data)) {
						matchFound = true;
						System.out.println("Enter inside the Method");
						System.out.println("Verified");
						break;
					}
				}
				if (!matchFound)
					// WebElement program.getText().toString();
					return Constants.KEYWORD_PASS + "  " + data + " "
							+ "is Not present in the Menu of SP";
			}
		} catch (Exception e) {
		}
		return Constants.KEYWORD_FAIL
				+ " : Element is Present in the List Expected Text is '"+" "
				+ (data) + "'";
	}

/**
	 * Purpose: To Verify the color code Usage : In This routine, take the CSS
	 * value of element using Xpath and store it into "colour" variable using
	 * CSS value it store the color value in RGB Format then after convert this
	 * RGB format into Hexadecimal format in "actualColor" variable. and also
	 * pass this color code value in Hexadecimal format in excel sheet in data
	 * variable. Validate this two value "actualColor" and "colour" using If
	 * condition
	 */

	@SuppressWarnings("deprecation")
	public String verifyBackgroundColor(String object, String data) {
		APP_LOGS.debug("verifyBackgroundColor");
		try {
			System.out.println("Data is " + data);

			String colour = driver
					.findElement(By.xpath(OR.getProperty(object))).getCssValue(
							"background-color");
			System.out
					.println("Value of color in RGB Fomrmat " + "  " + colour);
			String[] numbers = colour.replace("rgb(", "").replace(")", "")
					.split(",");
			int r = Integer.parseInt(numbers[0].trim());
			int g = Integer.parseInt(numbers[1].trim());
			int b = Integer.parseInt(numbers[2].trim());
			System.out.println("r: " + r + "g: " + g + "b: " + b);
			String actualColor = "#" + Integer.toHexString(r)
					+ Integer.toHexString(g) + Integer.toHexString(b);
			System.out.println("Value of color in HexaDecimal Format" + " "
					+ actualColor);
			if (actualColor.equalsIgnoreCase(data)) {
				System.out.println("Colour code from CSS Value" + " "
						+ actualColor + "Colour code from Data Sheet" + "  "
						+ data);
				return Constants.KEYWORD_PASS + " : "
						+ "Background Colour code from CSS Value :: " + "  " + actualColor
						+ "  " + "Colour code from Data Sheet :: " + "   "
						+ data;
			} else {
				return Constants.KEYWORD_FAIL + " : Color not Matched "
						+ (data) + "";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + e.getMessage();
		}
		// return Constants.KEYWORD_FAIL
		// +" : Element is not added in the List Expected Text is '"+(data)+"'";
	}
	
	@SuppressWarnings("deprecation")
	public String verifyColor(String object, String data) {
		APP_LOGS.debug("verifyColor");
		try {
			System.out.println("Data is " + data);

			String colour = driver
					.findElement(By.xpath(OR.getProperty(object))).getCssValue("color");
							
			System.out
					.println("Value of color in RGB Fomrmat " + "  " + colour);
			String[] numbers = colour.replace("rgb(", "").replace(")", "")
					.split(",");
			int r = Integer.parseInt(numbers[0].trim());
			int g = Integer.parseInt(numbers[1].trim());
			int b = Integer.parseInt(numbers[2].trim());
			System.out.println("r: " + r + "g: " + g + "b: " + b);
			String actualColor = "#" + Integer.toHexString(r)
					+ Integer.toHexString(g) + Integer.toHexString(b);
			System.out.println("Value of color in HexaDecimal Format" + " "
					+ actualColor);
			if (actualColor.equalsIgnoreCase(data)) {
				System.out.println("Colour code from CSS Value" + " "
						+ actualColor + "Colour code from Data Sheet" + "  "
						+ data);
				return Constants.KEYWORD_PASS + " : "
						+ "Background Colour code from CSS Value :: " + "  " + actualColor
						+ "  " + "Colour code from Data Sheet :: " + "   "
						+ data;
			} else {
				return Constants.KEYWORD_FAIL + " : Color not Matched "
						+ (data) + "";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + e.getMessage();
		}
		// return Constants.KEYWORD_FAIL
		// +" : Element is not added in the List Expected Text is '"+(data)+"'";
	}

	public String findBidreqno(String object, String data) {
		String bidreqnum;
		APP_LOGS.debug("findBidreqno" + object);
		try {
			String brnum = driver.findElement(By.xpath(OR.getProperty(object)))
					.getText();
			System.out.println(brnum);
			String string = brnum;
			String[] parts = string.split(" ");
			String part1 = parts[0]; // 004
			String part2 = parts[1];
			String part3 = parts[2];

			System.out.println(part1);
			System.out.println(part2);
			System.out.println(part3);

			bidreqnum = part3;
			System.out.println(bidreqnum);

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to get the Bid Request Number"
					+ e.getMessage();
		}

		return Constants.KEYWORD_PASS + "   ->      "
				+ " Bid Request Number in Info Tab -> " + bidreqnum;
	}

	public String writeInInput(String object, String data) {
		APP_LOGS.debug("Writing in text box");
		try {
			int dataLength = data.length();
			for (int i = 0; i <= dataLength - 1; i++) {
				char singleCharacter = data.charAt(i);
				String singleChar = Character.toString(singleCharacter);
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(
						singleChar);
				//driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			}
			System.out.println("Data is ------>>>>" + data);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Unable to write "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String write(String object, String data) {
		APP_LOGS.debug("Writing in text box");
		try {
			driver.findElements(By.xpath(OR.getProperty(object))).clear();
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(data);
			/*int dataLength = data.length();
			for (int i = 0; i <= dataLength - 1; i++) {
				char singleCharacter = data.charAt(i);
				String singleChar = Character.toString(singleCharacter);
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(
						singleChar);
				driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			}
			 */
			System.out.println("Data is ------>>>>" + data);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + " Unable to write "
			+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	/**
 	 * This Method used to get back to Main Content from Iframe.
 	 * Without this user cannot able to per form another task 
 	 * * Selenium. 
 	 */
	public String switchttoriginalcontent(String object, String data) {
		APP_LOGS.debug("Switching to original content");
		try {
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return Constants.KEYWORD_FAIL + " Unable to Switch "
			+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}
	
    /**
	 * 
	 * This Method is used to write text in Textbox Field. Using xpath we can
	 * locate the textbox field and using "sendKeys" method we would enter the
	 * data in the Textbox
	 */
	public String writeInput(String object, String data) {
		APP_LOGS.debug("Writing in text box");

		try {
			//driver.switchTo().frame("mainframe");
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(data);
			//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			System.out.println(data);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Unable to write "
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String writeIntInput(String object, String data) {
		APP_LOGS.debug("Writing in integer value in text box");
		System.out.println("Excecuting writeIntInput Keyword............");


		int integetData = Integer.parseInt(data);

		try {
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(
					"" + integetData);
		} catch (Exception e) {
			System.out.println("Unable to find element");
			return Constants.KEYWORD_FAIL + " Unable to write "
			+ e.getMessage();

		}
		return Constants.KEYWORD_PASS;

	}

	long l1;
	String s1 = "";

	// For mouse over to Element
	public String mouseOverToElementAction(String object, String data) {
		try {
			Actions actionObj = new Actions(driver);
			WebElement getElement = driver.findElement(By.xpath(OR
					.getProperty(object)));
			// actionObj.moveToElement(getElement).build().perform();
			actionObj.moveToElement(getElement, 0, 20).build().perform();
			// screenshotCaptue();
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to mouse over the element" + e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String mouseOverToElementPoint(String object, String data) {
		try {
			int xPosition = 0;
			Actions actionObj = new Actions(driver);
			WebElement getElement = driver.findElement(By.xpath(OR
					.getProperty(object)));
			Point elementLoc = getElement.getLocation();
			int MyData = Integer.parseInt(data);
			xPosition = 50 + MyData;
			int yPosition = 50;
			System.out.println("x point is" + xPosition + " y point is = "
					+ yPosition);
			actionObj.moveToElement(getElement, xPosition, yPosition);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to mouse over the element" + e.getMessage();
		}

		return Constants.KEYWORD_PASS;
	}

	public String screenshotCaptue(String object, String data) {
		try {

			Thread.sleep(5000);
			int count = 1;
			File srcFile = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(srcFile, new File(System.getProperty("user.dir")
					+ "//screenshots//" + count + ".jpg"));
			count++;
			System.out.println(count + "st screenshot of the page is captured");

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to take the screenshot"
					+ e.getMessage();
		}

		return Constants.KEYWORD_PASS;
	}

	// window scroll to the point
	public String scrollToTheElement(String object, String data) {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			WebElement ele = driver.findElement(By.xpath(OR.getProperty(object)));
			je.executeScript("arguments[0].scrollIntoView(true)", ele);

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to scroll down to the element"
					+ e.getMessage();
		}

		return Constants.KEYWORD_PASS;
	}

	/**
	 * Gets the time from Service Provider side.
	 * 
	 * @param object
	 * @param data
	 * @return
	 */
	public String findApptime(String object, String data) {
		try {
			String ele = driver.findElement(By.xpath(OR.getProperty(object)))
					.getText();
			System.out.println(ele);
			SPapptime = ele.substring(36, 52);
			CONFIG.setProperty("SPTime", SPapptime);
			System.out.println("Appointment Time form service Provider ::"
					+ SPapptime);
			String[] appsptlit = SPapptime.split("-");
			starttime1 = appsptlit[0];
			starttime2 = appsptlit[1];
			window1 = starttime1.replace("pm", " ").trim().toString();
			window2 = starttime2.replace("pm", " ").trim().toString();
			finalTime = window1+"-"+window2;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to perform"
					+ e.getMessage();
		}

		return Constants.KEYWORD_PASS
				+ " Appointment Time form service Provider ::" + finalTime;
	}

	
	/**
	 * Gets the time from customer' website and compare with the scheduled time from SP (apptime)
	 * @param object
	 * @param data
	 * @return
	 */
	public String vrfyschtme(String object, String data) {
		try {
			String custtme = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			System.out.println(custtme);
			String string = custtme;
			String[] parts = string.split(" ");
			String part1 = parts[0]; // 004
			String part2 = parts[1]; 
			String part3 = parts[2];
			String part4 = parts[3];
			String part5 = parts[4];
			String part6 = parts[5];
			String part7 = parts[6];
			String part8 = parts[7];
			String part9= parts[7];
			System.out.println(part1);
			System.out.println(part2);
			System.out.println(part3);
			System.out.println(part4);
			System.out.println(part5);
			System.out.println(part6);
			System.out.println(part7);
			System.out.println(part8);
			System.out.println(part9);
			date1 = part5+" "+part6+" "+part7;
			System.out.println(date1);
			if(AppTme.equals(date1)) {
				System.out.println("Schedule Time from SP- " +AppTme + "Scheduled time on customer" +date1 );
			}

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL+ " -- unable to verify"
					+ e.getMessage();
		}

		return Constants.KEYWORD_PASS + "    "+" Time Verified Successfully "+ "     " +" Schedule Time from Service Provider -> " +AppTme + " Scheduled time on customer " +date1;
	}

	/**
	 * Compare scheduled time from SP (AppTme) to scheduled time in email (date2)
	 * @param object
	 * @param data
	 * @return
	 */
	public String vrfygmailschtme(String object, String data) {
		try {
			
			if(AppTme.equalsIgnoreCase(date2)) {
				System.out.println("Appointment Time Given by Service Provider - " +AppTme + " Scheduled time captured in Gmail " +date2 );
			} else {
				return Constants.KEYWORD_FAIL+ " -- unable to verify "+"  Appointment Time Given by Service Provider ->  " +AppTme  + " Scheduled Appointment time captured in Gmail " +date2;
			}
		} catch (Exception e) {
		    return Constants.KEYWORD_FAIL+ " -- unable to verify"		+ e.getMessage();
		}
		return Constants.KEYWORD_PASS + "    "+" Time Verified Successfully ->:  "+ "     " +" Scheduled Time in Appeared in Gmail ->  " +date2 + " Appointment Time Given by service provider " +AppTme;
	}
	
	/**
	 * Compare scheduled time from SP (SPTime) to scheduled time in email (time)
	 * 
	 * @param object
	 * @param data
	 * @return
	 */
	public String vrfyApptme(String object, String data) {
		try {
			if (finalTime.equalsIgnoreCase(time)) {
				System.out
						.println("Appointment Time Given by Service Provider - "
								+ SPapptime
								+ " Scheduled time captured in Gmail " + time);
			} else {
				return Constants.KEYWORD_FAIL + " -- unable to verify "
						+ "  Appointment Time Given by Service Provider ->  "
						+ SPapptime
						+ " Scheduled Appointment time captured in Gmail "
						+ time;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to verify"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS + "    "
				+ " Time Verified Successfully ->:  " + "     "
				+ " Scheduled Time in Appeared in Gmail ->  " + time
				+ " Appointment Time Given by service provider " + finalTime;
	}
	
	/**
	 * Gets appointment time from customer website and stores it (in date3) for comparison later
	 * @param object
	 * @param data
	 * @return
	 */
	public String getcustapptme(String object, String data) {
		try {
			String custtme = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			System.out.println(custtme);
			String string = custtme;
			String[] parts = string.split(" ");
			String part1 = parts[0]; // 004
			String part2 = parts[1]; 
			String part3 = parts[2];
			String part4 = parts[3];
			String part5 = parts[4];
			String part6 = parts[5];
			String part7 = parts[6];
			String part8 = parts[7];
			String part9= parts[7];
			System.out.println(part1);
			System.out.println(part2);
			System.out.println(part3);
			System.out.println(part4);
			System.out.println(part5);
			System.out.println(part6);
			System.out.println(part7);
			System.out.println(part8);
			System.out.println(part9);
			date3 =part5+" "+part6+" "+part7;
			System.out.println(date3);


		} catch (Exception e) {
			return Constants.KEYWORD_FAIL+ " -- unable to verify"
					+ e.getMessage();
		}

		return Constants.KEYWORD_PASS + "    "+" Appointment Time on Customer Side ->:  "+ "     " +date3;
	}

	/**
	 * Gets appointment time from gmail and stores it (in date2) for comparison later
	 * @param object
	 * @param data
	 * @return
	 */
	public String getActNumFromGmail(String object, String data) {
		try {
			//String activityNumber = null;
			String custtme = driver.findElement(
					By.xpath(OR.getProperty(object))).getText();
			Pattern pattern = Pattern.compile("([0-9]{4})");
			Matcher matcher = pattern.matcher(custtme);
			String activityNumber = "";
			if (matcher.find()) {
				activityNumber = matcher.group(1);
			}
			/*
			 * System.out.println(custtme); String string = custtme; String[]
			 * parts = string.split(" "); String part1 = parts[0]; // 004 String
			 * part2 = parts[1]; String part3 = parts[2]; String part4 =
			 * parts[3]; String part5 = parts[4]; String part6 = parts[5];
			 * String part7 = parts[6]; String part8 = parts[7]; String part9 =
			 * parts[8]; String part10 = parts[9]; String part11 = parts[10];
			 * String part12 = parts[11]; String part13 = parts[12]; String
			 * part14 = parts[13]; String part15 = parts[14]; String part16 =
			 * parts[15]; System.out.println(part1); System.out.println(part2);
			 * System.out.println(part3); System.out.println(part4);
			 * System.out.println(part5); System.out.println(part6);
			 * System.out.println(part7); System.out.println(part8);
			 * System.out.println(part9); System.out.println(part10);
			 * System.out.println(part11); System.out.println(part12);
			 * System.out.println(part13); System.out.println(part14);
			 * System.out.println(part15); System.out.println(part16); actNum =
			 * part16;
			 */
			System.out.println(activityNumber);
			CONFIG.setProperty("Activity Num", activityNumber);
			System.out.println("Activity Number is ->" + activityNumber);

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to get the Activity Number" + e.getMessage();
		}
		return Constants.KEYWORD_PASS + "   -> "
				+ " Activity Number Captured from Gamil";
	}
	
	/**
 	 *Purpose: This method is used for to get the mobile number from
 	 *Email body.
 	 *Using Regex this method match the Pattern of the mobile number 
 	 *(971) 639-2267 in specified format and set the value of mobile 
 	 *number in config file.
 	 */
	public String getMobileNumFromGmail(String object, String data) {
		try {
			//String activityNumber = null;
			String custtme1 = driver.findElement(
					By.xpath(OR.getProperty(object))).getText();
			Pattern pattern = Pattern.compile("(\\([0-9]{3}\\)( )?[0-9]{3}-[0-9]{4})");
			Matcher matcher = pattern.matcher(custtme1);
			String MobileNumber = "";
			if (matcher.find()) {
				MobileNumber = matcher.group(1);
			}
			
			System.out.println(MobileNumber);
			CONFIG.setProperty("Mobile Num", MobileNumber);
			System.out.println("Mobile Number is ->" + MobileNumber);

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- unable to get the Mobile Number" + e.getMessage();
		}
		return Constants.KEYWORD_PASS + "   -> "
				+ " Mobile Number Captured from Gmail";
	}

	/**
	 * Purpose: This method is used for to Verify the mobile number from number.
	 * For verifying this method using the get the mobile number values from
	 * excel and get the value from the Config file which is set in
	 * getMobileNumFromGmail method
	 */

	public String vrfyMobileNumber(String object, String data) {
		APP_LOGS.debug("Verifying the text");
		try {
			expected = data;
			System.out.println("Data is -> " + data);
			if (CONFIG.getProperty("Mobile Num").equals(data)) {
				System.out.println("verified");
			} else {
				System.out.println("Object not found");
				return Constants.KEYWORD_FAIL;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " " + data + " object not found";
		}
		return Constants.KEYWORD_PASS;
	}

	public String verifyLinkText(String object, String data) {
		APP_LOGS.debug("Verifying link Text");
		try {
			String actual = driver
					.findElement(By.xpath(OR.getProperty(object))).getText();
			String expected = data;
			String confirmation = driver.findElement(
					By.xpath(OR.getProperty("confirmation"))).getText();
			if (actual.equals(expected))
				return Constants.KEYWORD_PASS + "--" + confirmation;
			else
				return Constants.KEYWORD_FAIL + " -- Link text not verified";

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Link text not verified"
					+ e.getMessage();
		}
	}
	
	public String getgmailschtme(String object, String data) {
		try {
			String custtme = driver.findElement(By.xpath(OR.getProperty(object))).getText();
			System.out.println(custtme);
			String string = custtme;
			String[] parts = string.split(" ");
			String part1 = parts[0]; // 004
			String part2 = parts[1]; 
			String part3 = parts[2];
			String part4 = parts[3];
			String part5 = parts[4];
			String part6 = parts[5];
			String part7 = parts[6];
			//String part8 = parts[7];
			//String part9= parts[7];
			System.out.println(part1);
			System.out.println(part2);
			System.out.println(part3);
			System.out.println(part4);
			System.out.println(part5);
			System.out.println(part6);
			System.out.println(part7);
			//System.out.println(part8);
			//System.out.println(part9);
			date2 = part5+" "+"to"+" "+part7;
			System.out.println(date2);
			if(AppTme.equalsIgnoreCase(date2)) {
				System.out.println("Scheduled Time in Appeared in Gmail - " +AppTme + " Scheduled time on customer Account " +date2 );
			} else {
				System.out.println("not verified");
			}
			
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL+ " -- unable to get the time"
					+ e.getMessage();
		}

		return Constants.KEYWORD_PASS + "   ->      " +" Scheduled Time Capture in Gamil -> " +date2 ;
	}

	/**
	 * Purpose: This method is used to get the current url using "grtCurrentUrl()" Method 
	 * Usage: At the time of BMW flow(Happy Path or Bypass) when Customer Survey page will
	 * open in the browser and using this method URL of Customer Survey will store in "APIURL" 
	 * variable.
	 * Date - 08/05/2018
	 */

	public String getCurrentURL(String object, String data) {
		try {
			APIURL = driver.getCurrentUrl();
			System.out.println(APIURL);
			APP_LOGS.debug(APIURL);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}
	
	/**
	 * Purpose: This method extracts the surveyid and guid and call runwebhook to complete the survey 
	 * example: https://survey.qmerit.com/survey/UZ0jfU?guid=091eb032-7063-4ada-b21c-74ccc2afe5c2
	 * survey id is the string between /survey/ and ?. 
	 * Date - 08/05/2018
	 */
	public String completeSurvey(String object, String data) {
		try {
			
			String urlstr = APIURL;
			String[] parts = urlstr.split("/survey/");
			String survey_guid = parts[1];
			String surveyid = survey_guid.split("\\?")[0];
			String guid = survey_guid.split("guid\\=")[1];
			System.out.println("surveyID:" + surveyid + ", GUID: " + guid);
			CONFIG.setProperty("GUID_VALUE", guid);
			if (data == null)
				data = CONFIG.getProperty("WalkThru15");
			switch (data) {
				case "15":
					if (this.runConsumerWebhook(surveyid, guid, data, "v1") == 0)
						return Constants.KEYWORD_FAIL + " -- Error completing the survey by webhook";
					break;					
				case "20":	
					if (this.runConsumerWebhook(surveyid, guid, data, "v2") == 0)
						return Constants.KEYWORD_FAIL + " -- Error completing the survey by webhook";
					break;
				case "closeout":	
					if (this.runCloseoutWebhook(surveyid, guid,data) == 0)
						return Constants.KEYWORD_FAIL + " -- Error completing the survey by webhook";
					break;
				default:
					return Constants.KEYWORD_FAIL + " -- No Survey Option selected";
				
				}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL+ " -- unable to Split the URL"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS ;
	}
	
	/**
	 * What is AutoIT? AutoIt V3 is a freeware tool which is used for automating
	 * anything in Windows environment. AutoIt script is written in a BASIC
	 * language. How It Works? write a script in SciTE script editor or in
	 * notepad and save the script with .au3 extension Now compile the .au3
	 * script using AutoIt Script to EXE converter, which converts .au3 file to
	 * .exe file In that editor provide source and destination folder location
	 * and click on convert button, it will create a .exe file. e.g. in
	 * (upload.exe) file in Upload folder,Wherever you encounter download/
	 * upload pop-up window in your Selenium test case, execute the .exe file
	 * Syntax to call .exe file in your script is:
	 * Runtime.getRuntime().exec(�path of exe file�);
	 *
	 */
	public String uploadUsingAutoIT(String object, String data) {
		try {

			Runtime.getRuntime().exec(
					System.getProperty("user.dir") +"\\Upload\\" + data);

			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}
	
	/**
	 * Purpose: Complete the Walkthru survey using webhook API. Pass in version v1 or v2 to run
	 *          appropriate version. For activity 1.5, run v1 version. For activities 2.0, run v2 
	 * Usage: to complete the survey, pass in surveyID and guid. Leave everything else as is.
	 * @param: surveyid, gUID 
	 * Date - 08/05/2018
	 */
	private int runConsumerWebhook(String surveyid, String gUID ,String data, String version) {

		String Rbody = "{" + "\"event_id\": \"01CC2A85Q99K9VH1NP89XB04NB\","
				+ "\"event_type\": \"form_response\"," + "\"form_response\": {"
				+ "  \"form_id\": \"" + surveyid + "\","
				+ "  \"token\": \"74ab8d5934c0055b7c01bf8c55bbba66\","
				+ "  \"submitted_at\": \"2018-04-27T01:07:32Z\","
				+ "  \"hidden\": {" 
				+ "      \"guid\": \"" + gUID + "\""
				+ "  },"
				// "    \"guid\": \"49996e8f-6cee-4807-a33f-277e500ba978\"" +
				+ "  \"calculated\": {"
				+ "    \"score\": 0"
				+ "  },"
				+ "  \"definition\": {"
				+ "    \"id\": \"UZ0jfU\","
				+ "    \"title\": \"QAB EV Readiness Review v3\","
				+ "    \"fields\": ["
				+ "      {"
				+ "        \"id\": \"aEuTygGGX3E6\","
				+ "        \"title\": \"Upload a picture of the installation location\","
				+ "        \"type\": \"file_upload\","
				+ "        \"ref\": \"9fcfacdca56f83e9\","
				+ "        \"allow_multiple_selections\": false,"
				+ "        \"allow_other_choice\": false"
				+ "      }"
				+ "    ]"
				+ "  },"
				+ "  \"answers\": ["
				+ "    {\r\n"
				+ "      \"type\": \"choice\","
				+ "      \"choice\": {"
				+ "        \"label\": \"Single-family home\""
				+ "      },"
				+ "      \"field\": {"
				+ "        \"id\": \"aEuTygGGX3E6\","
				+ "        \"type\": \"multiple_choice\""
				+ "      }"
				+ "    }"
				+ "  ]" + "}" + "}";
		System.out.println(Rbody);
		RestAssured.baseURI = CONFIG.getProperty("API_URL");
		RestAssured.port = 7550;
		RestAssured.basePath = "/" + version + "/Consumer/Webhook";
		System.out.println("basePath:" + RestAssured.basePath);
		RequestSpecification httpRequest = RestAssured.given();
		RequestSpecification contentType = httpRequest
				.contentType("application/json");
		RequestSpecification body = httpRequest.body(Rbody);
		Response response = httpRequest.request(Method.POST);
		if (response.statusCode() == 200) {
			System.out.println("Webhook successful");
			return 1;
		} else
			return 0;
	}


	/**
	 * Purpose: Complete the survey using webhook API. 
	 * Usage: to complete the survey, pass in surveyID and guid. Leave everything else as is.
	 * @param: surveyid, gUID 
	 * Date - 04/06/2018
	 * Created for Activity 2.0
	 */
	private int runCloseoutWebhook(String surveyid, String gUID ,String data) {

		String Rbody = "{" + "\"event_id\": \"01CC2A85Q99K9VH1NP89XB04NB\","
				+ "\"event_type\": \"form_response\"," + "\"form_response\": {"
				+ "  \"form_id\": \"" + surveyid + "\","
				+ "  \"token\": \"74ab8d5934c0055b7c01bf8c55bbba66\","
				+ "  \"submitted_at\": \"2018-04-27T01:07:32Z\","
				+ "  \"hidden\": {" 
				+ "      \"guid\": \"" + gUID + "\""
				+ "  },"
				// "    \"guid\": \"49996e8f-6cee-4807-a33f-277e500ba978\"" +
				+ "  \"calculated\": {"
				+ "    \"score\": 0"
				+ "  },"
				+ "  \"definition\": {"
				+ "    \"id\": \"UZ0jfU\","
				+ "    \"title\": \"QAB EV Readiness Review v3\","
				+ "    \"fields\": ["
				+ "      {"
				+ "        \"id\": \"aEuTygGGX3E6\","
				+ "        \"title\": \"Upload a picture of the installation location\","
				+ "        \"type\": \"file_upload\","
				+ "        \"ref\": \"9fcfacdca56f83e9\","
				+ "        \"allow_multiple_selections\": false,"
				+ "        \"allow_other_choice\": false"
				+ "      }"
				+ "    ]"
				+ "  },"
				+ "  \"answers\": ["
				+ "    {\r\n"
				+ "      \"type\": \"choice\","
				+ "      \"choice\": {"
				+ "        \"label\": \"Single-family home\""
				+ "      },"
				+ "      \"field\": {"
				+ "        \"id\": \"aEuTygGGX3E6\","
				+ "        \"type\": \"multiple_choice\""
				+ "      }"
				+ "    }"
				+ "  ]" + "}" + "}";
		System.out.println(Rbody);
		RestAssured.baseURI = CONFIG.getProperty("API_URL");
		RestAssured.port = 443;				//default port for https
		System.out.println("RestAssured.port: " + RestAssured.port);
		RestAssured.basePath = "/v1/TypeForm/Webhook";
		RequestSpecification httpRequest = RestAssured.given();
		RequestSpecification contentType = httpRequest
				.contentType("application/json");
		RequestSpecification body = httpRequest.body(Rbody);
		Response response = httpRequest.request(Method.POST);
		if (response.statusCode() == 200) {
			System.out.println("Close-out survey completed");
			return 1;
		}
		else
			return 0;
	}
	
	public String cleartxt(String object, String data) {
		APP_LOGS.debug("cleartxt");
		try {
			driver.findElement(By.xpath(OR.getProperty(object))).clear();
			System.out.println(driver.getTitle());
			//Thread.sleep(50000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Not able to clear";
		}
		return Constants.KEYWORD_PASS;
	}
	
	public String switchToMainContent(String object, String data) {
		APP_LOGS.debug("switchToMainContent");
		try {
			driver.switchTo().defaultContent();
			System.out.println(driver.getTitle());
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " Not able to click";
		}
		return Constants.KEYWORD_PASS;
	}

	public String clickLink(String object, String data) {
		APP_LOGS.debug("clickLink ");
		try {
			//driver.switchTo().frame(driver.findElement(By.xpath("//*[@id='testspecification_topics']/a[1]")));
			driver.findElement(By.linkText(OR.getProperty(object))).click();
			//	Thread.sleep(3000);
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to click on link"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	public String cal(String object, String data) {
		APP_LOGS.debug("cal ");
		try {
			//driver.switchTo().frame(driver.findElement(By.xpath("//*[@id='testspecification_topics']/a[1]")));
			//driver.findElement(By.linkText(OR.getProperty(object))).click();

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 2);

			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			//System.out.println(cal.getTime());
			//Output "Wed Sep 26 14:23:28 EST 2012"

			String formatted = format1.format(cal.getTime());
			System.out.println(formatted);
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(formatted);;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to click on link"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}


	// **************************************************************************
	public String privacySetting(String object, String data) {
		APP_LOGS.debug("Selecting the project privacy setting");

		try {
			driver.findElement(By.xpath(OR.getProperty("privacy_setting_icon")))
			.click();
			Thread.sleep(1000);

			if (data.equals(Constants.Public)) {
				driver.findElement(By.xpath(OR.getProperty("public_radio")))
				.click();
			} else if (data.equals(Constants.Subscribers)) {
				// driver.findElement(By.xpath(OR.getProperty("privacy_setting_icon"))).click();
				Thread.sleep(1000);
				driver.findElement(
						By.xpath(OR.getProperty("subscribers_radio"))).click();
				// driver.findElement(By.xpath(OR.getProperty("save_button"))).click();
			} else if (data.equals(Constants.Private)) {
				// driver.findElement(By.xpath(OR.getProperty("privacy_setting_icon"))).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath(OR.getProperty("private_radio")))
				.click();
				// driver.findElement(By.xpath(OR.getProperty("save_button"))).click();
			}
			driver.findElement(By.xpath(OR.getProperty("save_button"))).click();

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " - could not select the project privacy setting"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	// Applicaiton specific Keyword
	// For screenshot
	public void captureScreenshot(String filename, String keyword_execution_result) throws Exception

	{
		try{	// take screen shots
			if (CONFIG.getProperty("screenshot_everystep").equals("Y")) {
				// capturescreen
				File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") +"//screenshots//"+filename+".jpg"));
			}
			else if (keyword_execution_result.startsWith(Constants.KEYWORD_FAIL) && CONFIG.getProperty("screenshot_error").equals("Y") ) {
				// capture screenshot
				File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") +"//screenshots//"+filename+".jpg"));
			}
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}	
	
	/**
	 * Purpose: To Generate the Date of two weeks ago
	 *          Change from 14 to 30 days. The due date on all EV flow is now 30 days
	 * 
	 * @param: infodate, infomon Date - 19/06/2018 Created for Activity 2.0
	 */
	public String generate2weekDate(String infodate, String infomon) {
		APP_LOGS.debug("generate2weekDate ");
		try {
			//Backend time is in in UTC. Set timezone to UTC.
			TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 30);

			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MMM-dd");

			String formatted = format1.format(cal.getTime());
			System.out.println(formatted);
			String currDate = formatted;
			String[] dateParts = currDate.split("-");
			String year = dateParts[0];
			actduemon = dateParts[1];
			actDueDate = dateParts[2];

			System.out.println(year);
			System.out.println(actduemon);
			System.out.println(actDueDate);

			/*
			 * String infoDate =
			 * driver.findElement(By.xpath(OR.getProperty(object))).getText();
			 * System.out.println("Date captured in info Tab :" +infoDate);
			 * if(infoDate.equals(actDueDate)){ return Constants.KEYWORD_PASS
			 * +" Date Verified ::"+ actDueDate+ ": :"+infoDate; } else{ return
			 * Constants.KEYWORD_FAIL + " -- Not able to Verified the Date"; }
			 */
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to get the date"
					+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	/**
	 * Purpose: To Verify the Date for Due date
	 * 
	 * @param: actDueDate Date - 19/06/2018 Created for Activity 2.0
	 */
	public String verifyDueDate(String object, String data) {
		APP_LOGS.debug("Verifying the Date");
		try {
			this.generate2weekDate(actduemon, actDueDate);
			String infoDate = driver.findElement(
					By.xpath(OR.getProperty(object))).getText();
			System.out.println("Date captured in info Tab :" + infoDate);
			if (infoDate.equals(actDueDate)) {
				System.out.println("Verified Sucessfully");
				return Constants.KEYWORD_PASS + " Date Verified" + infoDate;
			} else {
				System.out.println(Constants.KEYWORD_FAIL
						+ " -- Not able to Verified the Date. Date Expected: " + actDueDate + " , actual date: " + infoDate);
				APP_LOGS.debug(Constants.KEYWORD_FAIL
						+ " -- Not able to Verified the Date. Date Expected: " + actDueDate + " , actual date: " + infoDate);
				return Constants.KEYWORD_FAIL						
						+ " -- Not able to Verified the Date. Date Expected: " + actDueDate + " , actual date: " + infoDate;
			}

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- Not able to click on link"
					+ e.getMessage();
		}

	}

	/**
	 * Purpose: To Verify the month for Due date
	 * 
	 * @param: actduemon Date - 19/06/2018 Created for Activity 2.0
	 */
	public String verifyDueMon(String object, String data) {
		APP_LOGS.debug("Verifying the Month ");
		try {
			this.generate2weekDate(actduemon, actDueDate);
			String actinfoMon = driver.findElement(
					By.xpath(OR.getProperty(object))).getText();
			System.out.println("Month captured in info Tab :" + actinfoMon);
			if (actinfoMon.equals(actduemon.toUpperCase())) {
				return Constants.KEYWORD_PASS + " Month Verified" + actinfoMon;
			} else {
				return Constants.KEYWORD_FAIL
						+ " -- Not able to Verified the Month";
			}

		} catch (Exception e) {
			return Constants.KEYWORD_FAIL
					+ " -- Not able to Verified the Month" + e.getMessage();
		}

	}

	/*
	 * Purpose: Select environment to test. Suite.xls file should have it as the top test to select the environment,
	 */
	public String select_environment(String object, String data) {
		String env = data.toLowerCase();
		try {
			switch(env) { 
			case "qa":
				CONFIG.setProperty("BMW_BASEURL", CONFIG.getProperty("BMW_BASEURL_QA"));
				CONFIG.setProperty("BMW_Cust_New_URL", CONFIG.getProperty("BMW_Cust_New_URL_QA"));
				CONFIG.setProperty("EV_Cust_New_URL", CONFIG.getProperty("EV_Cust_New_URL_QA"));
				CONFIG.setProperty("CP_Cust_New_URL", CONFIG.getProperty("CP_Cust_New_URL_QA"));
				CONFIG.setProperty("eMW_Cust_New_URL", CONFIG.getProperty("eMW_Cust_New_URL_QA"));
				CONFIG.setProperty("Mini_Cust_New_URL", CONFIG.getProperty("Mini_Cust_New_URL_QA"));
				CONFIG.setProperty("FLO_Cust_New_URL", CONFIG.getProperty("FLO_Cust_New_URL_QA"));
				CONFIG.setProperty("SP_URL", CONFIG.getProperty("SP_URL_QA"));
				CONFIG.setProperty("SP_New_URL", CONFIG.getProperty("SP_New_URL_QA"));
				CONFIG.setProperty("API_URL", CONFIG.getProperty("API_URL_QA"));
				break;
			case "qab":
				CONFIG.setProperty("BMW_BASEURL", CONFIG.getProperty("BMW_BASEURL_QAB"));
				CONFIG.setProperty("BMW_Cust_New_URL", CONFIG.getProperty("BMW_Cust_New_URL_QAB"));
				CONFIG.setProperty("EV_Cust_New_URL", CONFIG.getProperty("EV_Cust_New_URL_QAB"));
				CONFIG.setProperty("CP_Cust_New_URL", CONFIG.getProperty("CP_Cust_New_URL_QAB"));
				CONFIG.setProperty("eMW_Cust_New_URL", CONFIG.getProperty("eMW_Cust_New_URL_QAB"));
				CONFIG.setProperty("Mini_Cust_New_URL", CONFIG.getProperty("Mini_Cust_New_URL_QAB"));
				CONFIG.setProperty("FLO_Cust_New_URL", CONFIG.getProperty("FLO_Cust_New_URL_QAB"));
				CONFIG.setProperty("SP_URL", CONFIG.getProperty("SP_URL_QAB"));
				CONFIG.setProperty("SP_New_URL", CONFIG.getProperty("SP_New_URL_QAB"));
				CONFIG.setProperty("API_URL", CONFIG.getProperty("API_URL_QAB"));
				break;
			case "staging":
				CONFIG.setProperty("BMW_BASEURL", CONFIG.getProperty("BMW_BASEURL_STG"));
				CONFIG.setProperty("BMW_Cust_New_URL", CONFIG.getProperty("BMW_Cust_New_URL_STG"));
				CONFIG.setProperty("EV_Cust_New_URL", CONFIG.getProperty("EV_Cust_New_URL_STG"));
				CONFIG.setProperty("CP_Cust_New_URL", CONFIG.getProperty("CP_Cust_New_URL_STG"));
				CONFIG.setProperty("eMW_Cust_New_URL", CONFIG.getProperty("eMW_Cust_New_URL_STG"));
				CONFIG.setProperty("Mini_Cust_New_URL", CONFIG.getProperty("Mini_Cust_New_URL_STG"));
				CONFIG.setProperty("FLO_Cust_New_URL", CONFIG.getProperty("FLO_Cust_New_URL_STG"));
				CONFIG.setProperty("SP_URL", CONFIG.getProperty("SP_URL_STG"));
				CONFIG.setProperty("SP_New_URL", CONFIG.getProperty("SP_New_URL_STG"));
				CONFIG.setProperty("API_URL", CONFIG.getProperty("API_URL_STG"));
				break;
			case "prod1":
				CONFIG.setProperty("BMW_BASEURL", CONFIG.getProperty("BMW_BASEURL_PROD1"));
				CONFIG.setProperty("BMW_Cust_New_URL", CONFIG.getProperty("BMW_Cust_New_URL_PROD1"));
				CONFIG.setProperty("EV_Cust_New_URL", CONFIG.getProperty("EV_Cust_New_URL_PROD1"));
				CONFIG.setProperty("CP_Cust_New_URL", CONFIG.getProperty("CP_Cust_New_URL_PROD1"));
				CONFIG.setProperty("eMW_Cust_New_URL", CONFIG.getProperty("eMW_Cust_New_URL_PROD1"));
				CONFIG.setProperty("Mini_Cust_New_URL", CONFIG.getProperty("Mini_Cust_New_URL_PROD1"));
				CONFIG.setProperty("FLO_Cust_New_URL", CONFIG.getProperty("FLO_Cust_New_URL_PROD1"));
				CONFIG.setProperty("SP_URL", CONFIG.getProperty("SP_URL_PROD1"));
				CONFIG.setProperty("SP_New_URL", CONFIG.getProperty("SP_New_URL_PROD1"));
				CONFIG.setProperty("API_URL", CONFIG.getProperty("API_URL_PROD1"));
				break;
			case "prod2":
				CONFIG.setProperty("BMW_BASEURL", CONFIG.getProperty("BMW_BASEURL_PROD2"));
				CONFIG.setProperty("BMW_Cust_New_URL", CONFIG.getProperty("BMW_Cust_New_URL_PROD2"));
				CONFIG.setProperty("EV_Cust_New_URL", CONFIG.getProperty("EV_Cust_New_URL_PROD2"));
				CONFIG.setProperty("CP_Cust_New_URL", CONFIG.getProperty("CP_Cust_New_URL_PROD2"));
				CONFIG.setProperty("eMW_Cust_New_URL", CONFIG.getProperty("eMW_Cust_New_URL_PROD2"));
				CONFIG.setProperty("Mini_Cust_New_URL", CONFIG.getProperty("Mini_Cust_New_URL_PROD2"));
				CONFIG.setProperty("FLO_Cust_New_URL", CONFIG.getProperty("FLO_Cust_New_URL_PROD2"));
				CONFIG.setProperty("SP_URL", CONFIG.getProperty("SP_URL_PROD2"));
				CONFIG.setProperty("SP_New_URL", CONFIG.getProperty("SP_New_URL_PROD2"));
				CONFIG.setProperty("API_URL", CONFIG.getProperty("API_URL_PROD2"));
				break;				
			}
			//System.out.println(CONFIG.getProperty("EV_Cust_New_URL"));	
			return Constants.KEYWORD_PASS;
		} catch (Exception e)  {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}


	public String uploadFiled(String object, String data) {
		try {
			//StringSelection ss = new StringSelection(CONFIG.getProperty("upload"));
			StringSelection ss = new StringSelection(System.getProperty("user.dir") + CONFIG.getProperty("upload"));

			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

			//imitate mouse events like ENTER, CTRL+C, CTRL+V
			Robot robot = new Robot();
			// Press Enter
			robot.keyPress(KeyEvent.VK_ENTER);

			// Release Enter
			robot.keyRelease(KeyEvent.VK_ENTER);

			// Press CTRL+V
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			// Release CTRL+V
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			Thread.sleep(1000);

			// Press Enter 
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}

	/**
	 * This Method is used to upload any document using Robot Class
	 */
	public String upload_manage_req_doc(String object, String data) {
		try {
			//StringSelection ss = new StringSelection(CONFIG.getProperty("upload"));
			StringSelection ss = new StringSelection(System.getProperty("user.dir") +CONFIG.getProperty("upload_Manage_Req_Doc")+data);

			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

			//imitate mouse events like ENTER, CTRL+C, CTRL+V
			Robot robot = new Robot();
			// Press Enter
			robot.keyPress(KeyEvent.VK_ENTER);

			// Release Enter
			robot.keyRelease(KeyEvent.VK_ENTER);

			// Press CTRL+V
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);

			// Release CTRL+V
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			Thread.sleep(1000);

			// Press Enter 
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			APP_LOGS.debug("Uploaded DOC");
			return Constants.KEYWORD_PASS;
		} catch (Exception e) {
			APP_LOGS.debug("Fail to upload doc ");
			return Constants.KEYWORD_FAIL + e.getMessage();
		}
	}

	/**
	 * This Method is used to to cature the Activity number from info page
	 * @param object, data
	 */
	public String findActno(String object, String data) {
        String Actreqnum;
		APP_LOGS.debug("findActno: " + object);        
        try {
            String brnum = driver.findElement(By.xpath(OR.getProperty(object))).getText();
            System.out.println(brnum);
            String string = brnum;
            String[] parts = string.split(" ");
            String part1 = parts[0]; // 004
            String part2 = parts[1]; 
            
            System.out.println(part1);
            System.out.println(part2);
                        
            Actreqnum = part2;
            System.out.println(Actreqnum);
            
        } catch (Exception e) {
    		APP_LOGS.debug(Constants.KEYWORD_FAIL+ " -- unable to get the Activity Number");        
            return Constants.KEYWORD_FAIL+ " -- unable to get the Activity Number"
                    + e.getMessage();
        }
		APP_LOGS.debug("PASS");        
        return Constants.KEYWORD_PASS + "   ->      " +" Activity Number in Info Tab -> " +Actreqnum ;
    }
	
	public String noop(String object, String data) {
		return Constants.KEYWORD_PASS;
	}
	
	public String verifyPercentComplete(String object, String data) {
		String attrib = "";
		APP_LOGS.debug("verifyPercentComplete: " + object);
		try {
			attrib = driver.findElement(By.xpath(OR.getProperty(object)))
					.getAttribute("style");
			if (attrib.contains(data)) {
				return Constants.KEYWORD_PASS;
			} else {
				System.out.println(Constants.KEYWORD_FAIL + "expected:" + data
						+ ", actual:" + attrib);
				return Constants.KEYWORD_FAIL;
			}
		} catch (Exception e) {
			APP_LOGS.debug(Constants.KEYWORD_FAIL
					+ " -- unable to get percent complete");
			return Constants.KEYWORD_FAIL + "expected:" + data + ", actual:"
					+ attrib;
		}
	}
	/**
	 *Purpose : Search the requirement list to look for the expected requirement and verify the 
	 *          icon matches the expected icon
	 * Usage: 
	 * 	data - expected requirementment
	 *  icon - expected string representation of the icon
	 * Date: 9/4/18
	 */	
	private String verifyReq(String object, String data, String icon) {
		String reqText = "";
		try {
			System.out.println("verifyreqincomplete: Data is now = '" + data + "'");
			Thread.sleep(2000);
			List<WebElement> options =   driver.findElements(By.xpath(OR.getProperty(object)));
			// Loop through the options and select the one that matches
			System.out.println("Size is :" + options.size());

			if(options!=null && options.size()>0){
				boolean matchFound = false;
				for (WebElement opt : options) {
					System.out.println("opt is '" + (opt.getText()) + "'" + "; data is '" + (data) + "'");
					reqText = opt.getText().toString();
					if (reqText.contains((data))) {
						if (reqText.contains(icon)){
							matchFound = true;
							System.out.println("Found requirement and the icon matches (" + icon + ")");
						} else {
							matchFound = false;
							System.out.println("Found requirement but the icon does not match (" + icon + ")");							
						}
						break;
					}
				}
				if (!matchFound) {
					APP_LOGS.debug("requirement not found or icon not match (" + icon + ")");
					System.out.println("requirement not found or icon not match (" + icon + ")");
					return Constants.KEYWORD_FAIL + "  :  " + " requirement not incomplete or not found" + "  " + "Expected Text is '" +(data) + "'";					
				}
			} else {
				System.out.println("List is empty");
				return Constants.KEYWORD_FAIL + " No Element Found inside the list" + "   " + "List is Empty";
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + "Element not in the List"	+ e.getMessage();
		}
		return Constants.KEYWORD_PASS;
	}

	/**
	 *Purpose : Call verifyreq routine to look for the expected icon 
	 * Usage: 
	 * 	data - expected requirementment
	 * Date: 9/4/18
	 */		
	public String verifyReqIncomplete(String object, String data) {
		String icon = OR.getProperty("reqincomplete");
		return verifyReq(object, data, icon);
	}
	
	/**
	 *Purpose : Call verifyreq routine to look for the expected icon 
	 * Usage: 
	 * 	data - expected requirementment
	 * Date: 9/4/18
	 */		
	public String verifyReqcomplete(String object, String data) {
		String icon = OR.getProperty("reqcomplete");
		return verifyReq(object, data, icon);
	}
	
	/**
	 *Purpose : Call verifyreq routine to look for the expected icon 
	 * Usage: 
	 * 	data - expected requirementment
	 * Date: 9/4/18
	 */		
	public String verifyReqOveriden(String object, String data) {
		String icon = OR.getProperty("reqoverride");
		return verifyReq(object, data, icon);
	}
	
	/**
	 *Purpose : Verify the Appointment time from customer gmail inbox 
	 * Usage: 
	 * 	data - Gmailapptimearrpart1,Gmailapptimearrpart2
	 * Date: 9/7/18
	 */	
	public String VerifyGmailUiApptime(String object, String data) {
		try {
			//String Gmailapptime = driver.findElement(	By.xpath(OR.getProperty(object))).getText();
			System.out.println(time);
			String[] Gmailapptimearr = time.split("-");
			Gmailapptimearrpart1 = Gmailapptimearr[0].trim();
			Gmailapptimearrpart2 = Gmailapptimearr[1].replace("p", " ").trim();
			System.out.println(Gmailapptimearrpart1);
			System.out.println(Gmailapptimearrpart2);
			if ((spapptimearrpart1.equals(Gmailapptimearrpart1))
					&& (spapptimearrpart3.equals(Gmailapptimearrpart2))) {
				return Constants.KEYWORD_PASS + " Appointment Time Verified ::"
						+ "Appointmnet Time in service provider UI :: "+" "
						+ spapptimearrpart1 + " " + " and " + " "
						+ spapptimearrpart3 + " Appointmnet Time in Gmail UI ::"+ " "
						+ Gmailapptimearrpart1 + " " + " and " + " "
						+ Gmailapptimearrpart2;
			} else {
				return Constants.KEYWORD_FAIL
						+ "Appointment Time Not Verified ::"
						+ "Appointmnet Time in service provider UI :: "+ " "
						+ spapptimearrpart1 + " " + " and " + " "
						+ spapptimearrpart3 + "Appointmnet Time in Gmail UI :: "+" "
						+ Gmailapptimearrpart1 + " " + " and " + " "
						+ Gmailapptimearrpart2;
			}
		} catch (Exception e) {
			return Constants.KEYWORD_FAIL + " -- unable to get the time"
					+ e.getMessage();
		}
	}
	/**
	 *Purpose : Store Appoinment time from Service provider UI
	 *Usage: 
	 *data - spapptimearrpart1,spapptimearrpart3
	 * Date: 9/7/18
	 */	
		public String spUiApptime(String object, String data) {
			try {

				// String spapptime =
				// driver.findElement(By.xpath(OR.getProperty(object))).getText();
				System.out.println(SPapptime);
				String[] spapptimearr = SPapptime.split(" ");
				spapptimearrpart1 = spapptimearr[0].replace("pm", " ").trim();
				String spapptimearrpart2 = spapptimearr[1];
				spapptimearrpart3 = spapptimearr[2].replace("pm", " ").trim();
				System.out.println(spapptimearrpart1);
				System.out.println(spapptimearrpart2);
				System.out.println(spapptimearrpart3);
			} catch (Exception e) {
				return Constants.KEYWORD_FAIL + " -- unable to get the time"
						+ e.getMessage();
			}
			return Constants.KEYWORD_PASS
					+ "   ->      "
					+ " Scheduled Appointment Time Capture in Service provider UI -> "
					+ spapptimearrpart1 + "  " + spapptimearrpart3;
		}
		
		/**
		 *Purpose : This Method Store the value of Expiry date of requirement in
		 * format(2018-09-30) which is selected using calendar 
		 *Usage: 
		 *data - ExYear (Store the Value of Year- 2018),ExDate (Store the Value of Date 30)
		 * Date: 9/12/18
		 */	
		public String getExpiryDate(String object, String data) {
			APP_LOGS.debug("getExpiryDate ");
			try {
				expirydate =driver.findElement(By.xpath(OR.getProperty(object))).getAttribute("value");
				System.out.println("Expiry Date of the Requirment" +expirydate);
				String [] expiryDateArray = expirydate.split("-");
				ExYear = expiryDateArray[0].toString();
				ExMonth = expiryDateArray[1].toString();
				ExDate= expiryDateArray[2].toString();
				System.out.println("Expiry Date " + ExDate);
				System.out.println("Expiry Date " + ExMonth);
				System.out.println("Expiry Date " + ExYear);
			} catch (Exception e) {
				return Constants.KEYWORD_FAIL + " -- Not able to Get the Date"
						+ e.getMessage();
			}
			return Constants.KEYWORD_PASS +"Expiry Date of the Requirment selected from the Calenedar is :" 
					+ExYear+"-"+ExMonth+"-"+ExDate;
		}
		
		
		/**
		 *Purpose : This Method verify the expiry date of requirement after override 2018-09-30
		 *Usage: 
		 *data - savedexyear (Store the Value of Year- 2018),savedeexDate (Store the Value of Date 30)
		 * Date: 9/12/18
		 */	
		public String verifyExpiryDate(String object, String data) {
			APP_LOGS.debug("verifyExpiryDate ");
			try {
				
				String SavedExpirydate =driver.findElement(By.xpath(OR.getProperty(object))).getText();
				System.out.println("Expiry Date of the Requirment" +SavedExpirydate);
				String [] savedExpiryDateArray  = SavedExpirydate.split("-");
				savedexyear = savedExpiryDateArray[0].toString();
				savedexMonth = savedExpiryDateArray[1].toString();
				savedeexDate = savedExpiryDateArray[2].toString();
				System.out.println("Saved Expiry Year " +savedexyear);
				System.out.println("Saved Expiry Year " +savedexMonth);
				System.out.println("Saved Expiry Year " +savedeexDate);
				if(ExYear.equals(savedexyear) && ExDate.equals(savedeexDate)){
					System.out.println("Expiry Date verified");
					return Constants.KEYWORD_PASS +" Date Verified " +" Date Selected from Calendar "+ExYear+"-"+ExMonth+"-"+ExDate
							+" Date Appeared after Save" +savedexyear +"-"+"SEP"+"-"+savedeexDate;
				}else
				{
					System.out.println("Expiry Date not verified");
					return Constants.KEYWORD_FAIL+"Expiry Date not verified" ;
				}
				
			} catch (Exception e) {
				return Constants.KEYWORD_FAIL + " -- Not able to verified"
						+ e.getMessage();
			}
		}
}

	