package com.Frame.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Scanner;

import com.Frame.XL.Xls_Reader;
import com.Frame.script.Constants;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.utils.DateTimeUtil;



public class Report 

{
	private static ExtentReports extent;
	public static ExtentTest test1;

	public static  Xls_Reader suiteXLS;
	public static int currentSuiteID;
	public static String currentTestSuite;

    public static Xls_Reader currentTestSuiteXLS;
	public static int currentTestCaseID;
	public static String currentTestCaseName;
	public static int currentTestStepID;
	public static Properties CONFIG;
	public static Properties OR;
	public static String result_FolderName=null;	
	public static Date ExecutionStartTime;
	public static Date ExecutionEndTime;
	public static String OverallExecutionTime;
	
	
	
	/**
	 * @author Ashish Singh
	 * @return
	 * <p> This method is used to fill the 
	 * report on the basis of test Execution
	 * 
	 */	
	public static void FillReport(String TestCase,String Description)
	{
		
		String Result =currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.RESULT+1, currentTestStepID);
		String TSID=currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, "TSID", currentTestStepID);
		String TestData =FindData();
		if(TestData.isEmpty())
		{
			TestData="N";	
		}
		if(Result.startsWith("PASS")||Result.startsWith("Pass"))
		{
			
			//test1.log(LogStatus.PASS, Description );
			test1.log(LogStatus.PASS, TSID +": TestData : "+TestData+" Expected Result :"+Description+ "<br>"+"Actual Result :"+Result+"<br>");
			
			
		}else if(Result.isEmpty()||Result.startsWith("SKIP")||Result.startsWith("Skip"))
		{
			test1.log(LogStatus.SKIP, TSID +": Description :"+Description+": TestData : "+TestData );
		}
		else if(Result.startsWith("FAIL")||Result.startsWith("Fail"))
		{
			  test1.log(LogStatus.FAIL, TSID+"<br>"+"Actual Result :"+Result+"<br>"+"Expected Result :"+Description);
			  String path="..//screenshots//"+FindScreenshot();
			  if(path.contains("null"))
			  {
				  
			  }else{
				  test1.log(LogStatus.FAIL, TSID+test1.addScreenCapture(path));
				  
			  }
			  
			 //test.log(LogStatus.FAIL, TSID+test.addScreenCapture(path));
			  
			//  System.out.println("Screen is "+path);
			 
		  // test1.addScreenCapture(path);
		}
		
	}
	/**
	 * @author Ashish Singh
	 * @return
	 * <p> This method is used to find screenshots of Fail cases 
	 * and return name of screenshots
	 * 
	 */	
	public static String FindScreenshot() {
	    //System.out.println(currentTestSuite+"_"+currentTestCaseName+"_TS"+(currentTestStepID-1));
		
		File folder = new File(System.getProperty("user.dir")+"//screenshots");
		File[]  listOfFiles = folder.listFiles();
		if (listOfFiles == null) {
		    return "Error - Screenshot Not Found";
		}
		for (File file : listOfFiles) {
			String SreenshotName=file.getName();
	        if(SreenshotName.contains(currentTestSuite+"_"+currentTestCaseName+"_TS"+(currentTestStepID-1))) {
	            // System.out.println();
	        	System.out.println(currentTestSuite+"_"+currentTestCaseName+"_TS"+(currentTestStepID-1));
	            //System.out.println("path is "+file.getAbsolutePath());
	        	//System.out.println("path is "+file.getName());
	        	return file.getName();
	        	//return file.getAbsolutePath();
	        }
	     }
		return  "Error - Screenshot Not Found";
	}
	/**
	 * @author Ashish Singh
	 * @return
	 * <p> This method is used to find the Test data
	 * of the cureent test case
     * */	
		public static String FindData()
	{
		String data = null;
		data=currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.DATA,currentTestStepID  );
		//System.out.println("Data is on"+data);
		if(data.startsWith(Constants.DATA_START_COL))
		{
			if(currentTestSuiteXLS.isSheetExist(currentTestCaseName))
	    	{
			if(currentTestSuiteXLS.getCellData(currentTestCaseName, Constants.RUNMODE, 2).equalsIgnoreCase(Constants.RUNMODE_YES))
			{
				data=currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.DATA,currentTestStepID  );
				if(data.startsWith(Constants.DATA_START_COL))
				{
					// read actual data value from the corresponding column				
					data=currentTestSuiteXLS.getCellData(currentTestCaseName, data.split(Constants.DATA_SPLIT)[1] ,2 );
					return data;
			    }
			}
	    	}
		}
		else if(data.startsWith(Constants.CONFIG))
		{
			
			data=CONFIG.getProperty(data.split(Constants.DATA_SPLIT)[1]);
			return data;
		}else if(data.isEmpty())
		{
			
			return "N";
		}
		//if data row blank then return N;
		return "N";
	}


	public static String ReportPath()
	{
		
		Date d = new Date();
		String date=d.toString().replaceAll(" ", "_");
		date=date.replaceAll(":", "_");
		date=date.replaceAll("\\+", "_");
		System.out.println(date);
	    result_FolderName="Reports"+"_"+date;
		String reportsDirPath=System.getProperty("user.dir")+"\\Reports";
		new File(result_FolderName).mkdirs();
		//String ReportName=CONFIG.getProperty("ReportName");		
		String ReportPath=result_FolderName+"\\"+"Automation_Report_"+date+".html";
        return ReportPath;
	}
	
	public static void SetEnvironment()
	{
		String environment=CONFIG.getProperty("environment");
		String release=CONFIG.getProperty("release");
		//String UserName=CONFIG.getProperty("UserName");
		//String OS=CONFIG.getProperty("OS");
				
		extent.addSystemInfo("Environment", environment);
		extent.addSystemInfo("Release Build", release);
		//extent.addSystemInfo("User Name", UserName);
		//extent.addSystemInfo("OS", OS);
		
		/*extent.addSystemInfo("<b>"+"Execution Start time", ExecutionStartTime.toString());
		extent.addSystemInfo("<b>"+"Execution End time", ExecutionEndTime.toString());
		extent.addSystemInfo("<b>"+"Total Execution Time", OverallExecutionTime.toString());
	*/	
		
		

	}
	static File file;
	static FileWriter fw;
	static BufferedWriter bw;
	public static void SetExecutionStartTime() throws Exception
	{
		
		file = new File("Report.txt");
        file.createNewFile();
        fw = new FileWriter(file);
        bw = new BufferedWriter(fw);
        ExecutionStartTime=Calendar.getInstance().getTime();
        bw.write(ExecutionStartTime+"\r\n");
        bw.flush();
		
	}
	public static void SetExecutionEndime() throws Exception
	{
		ExecutionEndTime=Calendar.getInstance().getTime();
		String TotalTime=DateTimeUtil.getDiff(ExecutionEndTime, ExecutionStartTime);
        bw.write(ExecutionEndTime+"\r\n"+TotalTime);
        bw.flush();
        bw.close();	
	}
	

	public static void GetExecutionTime() throws Exception
	{
		String ReportTime =System.getProperty("user.dir")+"//Report.txt";
    	Scanner ReportFile = new Scanner(new File(ReportTime));
        List<String> tokens = new ArrayList<String>();
        while (ReportFile.hasNext()) {
            tokens.add(ReportFile.nextLine());
        }
        System.out.println(tokens.get(0));
        System.out.println(tokens.get(1));
        System.out.println(tokens.get(2));
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		ExecutionStartTime=formatter.parse(tokens.get(0));
		ExecutionEndTime=formatter.parse(tokens.get(1));
		OverallExecutionTime=tokens.get(2);
		//test1.setStartedTime(ExecutionStartTime);
		//test1.setEndedTime(ExecutionEndTime);
		
		
	
	}
		
		
	
  public static void main(String[] args) throws IOException
     {
	try{
		
	System.out.println("Report Started");	
	
	FileInputStream fs = new FileInputStream(System.getProperty("user.dir")+"//src//com//Frame//config//config.properties");
	CONFIG= new Properties();
	CONFIG.load(fs);

	fs = new FileInputStream(System.getProperty("user.dir")+"//src//com//Frame//config//or.properties");
	OR= new Properties();
	OR.load(fs);
	//suiteXLS = new Xls_Reader(System.getProperty("user.dir")+"//Suite.xlsx");
	 suiteXLS = new Xls_Reader(System.getProperty("user.dir")+"//src//com//Frame//excelfiles//Suite.xls");
	 extent = new ExtentReports(ReportPath(), true);
	 extent.loadConfig(new File(System.getProperty("user.dir")+"//extent-config.xml"));
	/// GetExecutionTime();
	 SetEnvironment();
	
	
	for(currentSuiteID=2;currentSuiteID<=suiteXLS.getRowCount(Constants.TEST_SUITE_SHEET);currentSuiteID++)
	{
	
	 currentTestSuite=suiteXLS.getCellData(Constants.TEST_SUITE_SHEET, Constants.Test_Suite_ID, currentSuiteID);
	 
		if(suiteXLS.getCellData(Constants.TEST_SUITE_SHEET, Constants.RUNMODE, currentSuiteID).equalsIgnoreCase(Constants.RUNMODE_YES))
		{
			
		//	currentTestSuiteXLS=new Xls_Reader(System.getProperty("user.dir")+"//"+currentTestSuite+".xlsx");
			currentTestSuiteXLS=new Xls_Reader(System.getProperty("user.dir")+"//src//com//Frame//excelfiles//"+currentTestSuite+".xls");
			for(currentTestCaseID=2;currentTestCaseID<=currentTestSuiteXLS.getRowCount("Test Cases");currentTestCaseID++)
			{			
				currentTestCaseName=currentTestSuiteXLS.getCellData(Constants.TEST_CASES_SHEET, Constants.TCID, currentTestCaseID);
				
				String currentTestCaseDesc=currentTestSuiteXLS.getCellData(Constants.TEST_CASES_SHEET, Constants.DESCRIPTION, currentTestCaseID);
				//System.out.println("helloo");
				//System.out.println(currentTestCaseName + "-"+currentTestCaseDesc);
								
				if(currentTestSuiteXLS.getCellData(Constants.TEST_CASES_SHEET, Constants.RUNMODE, currentTestCaseID).equalsIgnoreCase(Constants.RUNMODE_YES))
				{
					 String TestDescription =currentTestSuiteXLS.getCellData(Constants.TEST_CASES_SHEET, Constants.DESCRIPTION, currentTestCaseID);
					 test1 =extent.startTest(currentTestCaseName, TestDescription);
					 
					 
					 test1.assignCategory(currentTestSuite);
					 
					for(currentTestStepID=2;currentTestStepID<=currentTestSuiteXLS.getRowCount(Constants.TEST_STEPS_SHEET);currentTestStepID++)
					{
						 if(currentTestCaseName.equals(currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.TCID, currentTestStepID)))
						 {
							
							 
							 String TestCaseName =currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.TCID, currentTestStepID);
		 			    	 String TestStepDescription =currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.DESCRIPTION, currentTestStepID);
		 			    	// System.out.println(TestCaseName +"="+TestDescription);
		 			    	
		 			    	 FillReport(TestCaseName,TestStepDescription);
		 			    	//String data=currentTestSuiteXLS.getCellData(Constants.TEST_STEPS_SHEET, Constants.DATA,currentTestStepID  );
		 				 }
					
					}
					extent.endTest(test1);
		      		extent.flush();
						
					
				}
			}
		}
	}
	//GetExecutionTime();
	extent.flush();
	System.out.println("Report Finish");
}
	catch(Exception e)
	{
		System.out.println("error "+e.getMessage());
	}
	
	
}
}