package com.Frame.XL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import com.sun.istack.internal.NotNull;

public class XlsWriter {
	public String path;
	public FileInputStream fis = null;
	public FileOutputStream fileOut = null;
	private HSSFWorkbook workbook = null;
	private HSSFSheet sheet = null;

	public XlsWriter(@NotNull String path, @NotNull String sheetName) {
		this.path = path;
		try {
			fis = new FileInputStream(path);
			workbook = new HSSFWorkbook(fis);
			sheet = workbook.getSheet(sheetName);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void writeExcel(String data) {
		try {
			if (sheet != null) {
				int rowCount = sheet.getPhysicalNumberOfRows();
				Row row;
				Cell cell;
				if (rowCount > 1) {
					row = sheet.getRow(rowCount - 1);
					cell = row.getCell(0);
					if (cell == null)
						cell = row.createCell(0);
				} else {
					row = sheet.createRow(rowCount);
					cell = row.createCell(0);
				}
				cell.setCellValue(data);
				FileOutputStream fos = new FileOutputStream(new File(path));
				workbook.write(fos);
				workbook.close();
			} else
				return;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}
	public void writeExcel1(String data) {
		try {
			if (sheet != null) {
				int rowCount = sheet.getPhysicalNumberOfRows();
				Row row;
				Cell cell;
				if (rowCount > 1) {
					row = sheet.getRow(rowCount - 1);
					cell = row.getCell(1);
					if (cell == null)
						cell = row.createCell(1);
				} else {
					row = sheet.createRow(rowCount);
					cell = row.createCell(1);
				}
				cell.setCellValue(data);
				FileOutputStream fos = new FileOutputStream(new File(path));
				workbook.write(fos);
				workbook.close();
			} else
				return;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}